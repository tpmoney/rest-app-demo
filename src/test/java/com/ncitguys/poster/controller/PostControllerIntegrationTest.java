package com.ncitguys.poster.controller;

import static com.ncitguys.poster.utils.IntegrationData.*;
import static com.ncitguys.poster.utils.TestUtils.validateErrorResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.*;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.db.entity.Comment;
import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.utils.TestUtils;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Timeout(30)
public class PostControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper jsonMapper;

    @BeforeEach
    void initDb() {
        jdbcTemplate.update("TRUNCATE posts CASCADE; TRUNCATE comments CASCADE;", Map.of());
    }

    // *************************************************************
    // Tests dealing with Posts
    // *************************************************************

    /**
     * Validate that uploading a new post returns a 200 response
     * with the new post id
     *
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @Test
    void postAPostWorks() throws JsonMappingException, JsonProcessingException {
        final var url = makeUrl("/posts");
        final ResponseEntity<String> response = restTemplate.postForEntity(url, NEW_POST_1,
                String.class);

        assertThat(response.getStatusCode()).isEqualTo(CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getHeaders().getLocation()).satisfies(loc -> {
            final var path = loc.getPath();
            assertThat(path).matches("\\./\\d+");
        });

        final var post = jsonMapper.readValue(response.getBody(), Post.class);
        assertThat(post.id()).isNotNull();
        assertThat(post.author()).isEqualTo(NEW_POST_1.author());
        assertThat(post.content()).isEqualTo(NEW_POST_1.content());
        assertThat(post.published()).isTrue();

        final var now = OffsetDateTime.now();
        assertThat(post.postedDate()).isBetween(now.minusMinutes(1), now.plusMinutes(1));
        assertThat(post.lastEdited()).isEqualTo(post.postedDate());

        assertThat(countPosts()).isEqualTo(1);
    }

    /**
     * Validate that a new post is rejected with a 400 response
     * if the author is missing
     */
    @ParameterizedTest(name = "postAPostFailsIfAuthorIsMissing - Author: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void postAPostFailsIfAuthorIsMissing(final String author) {
        final var url = makeUrl("/posts");
        final var newPost = NEW_POST_1.toBuilder()
                .author(author)
                .build();
        final ResponseEntity<String> response = restTemplate.postForEntity(url, newPost, String.class);

        validateErrorResponse(response, BAD_REQUEST, "Post author cannot be empty");

        assertThat(countPosts()).isEqualTo(0);
    }

    /**
     * Validate that a new post is rejected with a 400 response
     * if the content is missing
     */
    @ParameterizedTest(name = "postAPostFailsIfContentIsMissing - Content: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void postAPostFailsIfContentIsMissing(final String content) {
        final var url = makeUrl("/posts");
        final var newPost = NEW_POST_1.toBuilder()
                .content(content)
                .build();
        final ResponseEntity<String> response = restTemplate.postForEntity(url, newPost, String.class);

        validateErrorResponse(response, BAD_REQUEST, "Post content cannot be empty");

        assertThat(countPosts()).isEqualTo(0);
    }

    /**
     * Validate that GETing a post by ID works and returns the post
     */
    @Test
    void getPostByIdWorks() {
        final var newPostId = loadPosts(NEW_POST_1).getFirst().id();
        final var url = makeUrl("/posts/" + newPostId);
        final ResponseEntity<Post> response = restTemplate.getForEntity(url, Post.class);

        assertThat(response.getStatusCode()).isEqualTo(OK);
        final var body = response.getBody();
        assertThat(body.id()).isEqualTo(newPostId);
        assertThat(body.author()).isEqualTo(NEW_POST_1.author());
        assertThat(body.content()).isEqualTo(NEW_POST_1.content());
        assertThat(body.published()).isTrue();
    }

    /**
     * Validate that a GET request for a non-existent post
     * returns a 404 reponse
     */
    @Test
    void getPostByIdFails() {
        final var url = makeUrl("/posts/" + Integer.MAX_VALUE);
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        validateErrorResponse(response, NOT_FOUND, "Post with ID " + Integer.MAX_VALUE + " not found");
    }

    /**
     * Validate that a GET request for all posts by author
     * returns a list of posts by that author and not
     * any others
     *
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @ParameterizedTest(name = "getPostsByAuthorWorks - Author: \"{0}\"")
    @ValueSource(strings = { AUTHOR_1, AUTHOR_2 })
    void getPostsByAuthorWorks(final String author) throws JsonMappingException, JsonProcessingException {
        loadPosts(NEW_POST_1, NEW_POST_2, NEW_POST_3, NEW_POST_4);

        final var url = makeUrl("/posts/author/" + author);
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertThat(response.getStatusCode()).isEqualTo(OK);
        final var body = response.getBody();
        assertThat(body).isNotBlank();

        final var posts = jsonMapper.readValue(body, Post[].class);
        assertThat(posts).hasSize(2);
        assertThat(posts)
                .allSatisfy(post -> post.author().equals(author))
                .anySatisfy(Post::published);
    }

    /**
     * Validate that GET for all posts by an author does not return
     * any unpublished posts
     *
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @Test
    void getPostsByAuthorDoesNotReturnUnpublishedPosts() throws JsonMappingException, JsonProcessingException {
        final var uploaded = loadPosts(NEW_POST_1, NEW_POST_2);
        final var unpublishedId = uploaded.getFirst().id();
        jdbcTemplate.update("UPDATE posts SET published = false WHERE id = :id",
                Map.of("id", unpublishedId));

        final var url = makeUrl("/posts/author/" + NEW_POST_1.author());
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody()).isNotBlank();
        final var posts = jsonMapper.readValue(response.getBody(), Post[].class);

        assertThat(posts)
                .hasSize(1)
                .allSatisfy(post -> {
                    assertThat(post.author()).isEqualTo(NEW_POST_1.author());
                    assertThat(post.published()).isTrue();
                    assertThat(post.id()).isNotEqualTo(unpublishedId);
                });

        jdbcTemplate.update("UPDATE posts SET published = true WHERE author = :author",
                Map.of("author", NEW_POST_1.author()));
    }

    /**
     * Validate that GET posts by author returns an empty list
     * if there are no posts by the author
     */
    @Test
    void getPostsByAuthorReturnsEmptyListWhenNoAuthor() {
        loadPosts(NEW_POST_1, NEW_POST_2, NEW_POST_3, NEW_POST_4);
        final var url = makeUrl("/posts/author/MarkTwain");

        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody()).isNotBlank();
        assertThat(response.getBody()).isEqualTo("[]");
    }

    /**
     * Validate that DELETEing a post by ID unpublishes the post
     */
    @Test
    void deletePostByIdUnpublishesPost() {
        final var uploaded = loadPosts(NEW_POST_1, NEW_POST_2);
        final var changePostId = uploaded.getFirst().id();

        final var url = makeUrl("/posts/" + changePostId);
        final ResponseEntity<Void> response = restTemplate.exchange(url, DELETE, HttpEntity.EMPTY, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(OK);

        ResponseEntity<?> getResponse = restTemplate.getForEntity(url, String.class);
        assertThat(getResponse.getStatusCode()).isEqualTo(NOT_FOUND);

        // Validating we didn't delete the post from the db
        assertThat(countPosts()).isEqualTo(2);

        jdbcTemplate.update("UPDATE posts SET published = true WHERE id = :id",
                Map.of("id", changePostId));

        getResponse = restTemplate.getForEntity(url, Post.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody())
                .isInstanceOfSatisfying(Post.class, p -> {
                    assertThat(p.id()).isEqualTo(changePostId);
                    assertThat(p.content()).isEqualTo(NEW_POST_1.content());
                });
    }

    /**
     * Validate that DELETEing a post by ID returns a 404 if
     * the post doesn't exist
     */
    @Test
    void deletePostByIdReturns404IfNotFound() {
        final var url = makeUrl("/posts/9000");
        final ResponseEntity<Void> response = restTemplate.exchange(url, DELETE, HttpEntity.EMPTY, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    /**
     * Validate that PUT requests to posts update the post correctly
     */
    @Test
    void updatePostUpdatesPost() {
        final var uploaded = loadPosts(NEW_POST_1, NEW_POST_2);
        final var changePostId = uploaded.getFirst().id();

        final var updatedPost = NEW_POST_1.toBuilder()
                .content("A very important edit!")
                .build();

        final var updateRequest = new HttpEntity<>(updatedPost);
        var url = makeUrl("/posts/" + changePostId);
        final ResponseEntity<Void> response = restTemplate.exchange(url, PUT, updateRequest, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(NO_CONTENT);
        url = makeUrl("/posts/author/" + NEW_POST_1.author());
        final var getResponse = restTemplate.getForEntity(url, Post[].class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody())
                .hasSize(2)
                .anySatisfy(p -> {
                    assertThat(p.id()).isEqualTo(changePostId);
                    assertThat(p.content()).isEqualTo(updatedPost.content());
                    assertThat(p.lastEdited()).isAfter(p.postedDate());
                })
                .anySatisfy(p -> {
                    assertThat(p.id()).isNotEqualTo(changePostId);
                    assertThat(p.content()).isEqualTo(NEW_POST_2.content());
                    assertThat(p.lastEdited()).isEqualTo(p.postedDate());
                });
    }

    /**
     * Validate that PUT requests to posts return a NOT_FOUND if
     * the post doesn't exist
     */
    @Test
    void updatePostReturnsNotFoundIfNotFound() {
        final var updateRequest = new HttpEntity<>(NEW_POST_1);
        final var url = makeUrl("/posts/9000");
        final ResponseEntity<Void> response = restTemplate.exchange(url, PUT, updateRequest, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    /**
     * Validate that PUT requests to posts return a BAD_REQUEST if
     * the new content is empty
     */
    @ParameterizedTest(name = "updatePostReturnsBadRequestIfEmptyContent - Content: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void updatePostReturnsBadRequestIfEmptyContent(final String content) {
        final var originalPost = loadPosts(NEW_POST_1).getFirst();
        final var updatedPost = NEW_POST_1.toBuilder()
                .content(content)
                .build();

        final var updateRequest = new HttpEntity<>(updatedPost);
        final var url = makeUrl("/posts/" + originalPost.id());
        final ResponseEntity<String> response = restTemplate.exchange(url, PUT, updateRequest, String.class);

        validateErrorResponse(response, BAD_REQUEST, "Post content cannot be empty");

        final var getResponse = restTemplate.getForEntity(url, Post.class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody()).isEqualTo(originalPost);
    }

    /**
     * Validate that PUT requests to posts return a BAD_REQUEST if
     * the author is empty or if it's not the author of the post
     */
    @ParameterizedTest(name = "updatePostReturnsBadRequestIfWrongAuthor - Author: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    @ValueSource(strings = { "MarkTwain" })
    void updatePostReturnsBadRequestIfWrongAuthor(final String author) {
        final var originalPost = loadPosts(NEW_POST_1).getFirst();
        final var updatedPost = NEW_POST_1.toBuilder()
                .author(author)
                .build();

        final var updateRequest = new HttpEntity<>(updatedPost);
        final var url = makeUrl("/posts/" + originalPost.id());
        final ResponseEntity<String> response = restTemplate.exchange(url, PUT, updateRequest, String.class);

        validateErrorResponse(response, BAD_REQUEST, "is not the author of post " + originalPost.id());

        final var getResponse = restTemplate.getForEntity(url, Post.class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody()).isEqualTo(originalPost);
    }

    // **********************************************************************
    // Tests dealing with Comments
    // **********************************************************************

    /**
     * Validate that POST requests to comments works
     * and returns a 201 CREATED response with location
     */
    @Test
    void testUploadComment() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var response = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class);

        assertThat(response.getStatusCode()).isEqualTo(CREATED);
        assertThat(response.getHeaders().getLocation())
                .isNotNull()
                .satisfies(loc -> {
                    assertThat(loc.getPath()).matches("\\./\\d+");
                });

        final var comment = response.getBody();
        assertThat(comment).isNotNull();
        assertThat(comment.author()).isEqualTo(NEW_COMMENT_1.author());
        assertThat(comment.content()).isEqualTo(NEW_COMMENT_1.content());
        assertThat(comment.commentOn()).isEqualTo(post.id());
        assertThat(comment.replyTo()).isNull();
        assertThat(comment.published()).isTrue();
        final var now = OffsetDateTime.now();
        assertThat(comment.postedDate()).isBetween(now.minusMinutes(1), now.plusMinutes(1));
        assertThat(comment.lastEdited()).isEqualTo(comment.postedDate());
        assertThat(comment.lastEdited()).isBetween(now.minusMinutes(1), now.plusMinutes(1));
    }

    /**
     * Validate that POST requests to comments returns a BAD_REQUEST
     * if the content of the comment is empty and doesn't try to save
     * the comment
     */
    @ParameterizedTest(name = "testUploadCommentReturnsBadRequestIfEmptyContent - Content: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void testUploadCommentReturnsBadRequestIfEmptyContent(final String content) {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");

        final var emptyComment = NEW_COMMENT_1.toBuilder()
                .content(content)
                .build();
        final ResponseEntity<String> response = restTemplate.postForEntity(url, emptyComment, String.class);

        validateErrorResponse(response, BAD_REQUEST, "Comment content cannot be empty");

        assertThat(countComments()).isEqualTo(0);
    }

    /**
     * Validate that POST requests to comments return a BAD_REQUEST
     * if the author of the comment is empty and doesn't try to save
     * the comment
     */
    @ParameterizedTest(name = "testUploadCommentReturnsBadRequestIfEmptyAuthor - Author: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void testUploadCommentReturnsBadRequestIfEmptyAuthor(final String author) {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var emptyComment = NEW_COMMENT_1.toBuilder()
                .author(author)
                .build();
        final ResponseEntity<String> response = restTemplate.postForEntity(url, emptyComment, String.class);

        validateErrorResponse(response, BAD_REQUEST, "Comment author cannot be empty");

        assertThat(countComments()).isEqualTo(0);
    }

    /**
     * Validate that POST requests to comments return a NOT_FOUND
     * if the post we're attempting to comment on doesn't exist
     */
    @Test
    void testUploadCommentReturnsNotFoundIfPostDoesNotExist() {
        loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/9000/comments");
        final ResponseEntity<String> response = restTemplate.postForEntity(url, NEW_COMMENT_1, String.class);

        validateErrorResponse(response, NOT_FOUND, "Cannot add comment to non-existent post");

        assertThat(countComments()).isEqualTo(0);
    }

    /**
     * Validate that POST requests to comments return a BAD_REQUEST
     * if the comment we're replying to is for a different post
     */
    @Test
    void testUploadCommentReturnsBadRequestIfReplyToIsForDifferentPost() {
        final var posts = loadPosts(NEW_POST_1, NEW_POST_2);
        var url = makeUrl("/posts/" + posts.get(0).id() + "/comments");
        final var response = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class);
        final var replyId = response.getBody().id();
        assertThat(replyId).isNotNull();
        assertThat(countComments()).isEqualTo(1);

        final var reply = NEW_COMMENT_2.toBuilder()
                .replyTo(replyId)
                .build();

        url = makeUrl("/posts/" + posts.get(1).id() + "/comments");
        final ResponseEntity<String> badResponse = restTemplate.postForEntity(url, reply, String.class);

        validateErrorResponse(badResponse, BAD_REQUEST, "replyTo value is not a comment on this post");

        assertThat(countComments()).isEqualTo(1);
    }

    /**
     * Validate that GET requests to /posts/{id}/comments
     * returns all comments on the post in a heirarchical list
     * <p/>
     * This should return a hierarchy of comments where
     * the top level comments are 1 and 3 and comment 2
     * is a child of 1.
     */
    @Test
    void testGetComments() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");

        final var firstComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        // Add reply to first comment
        final var replyTo = NEW_COMMENT_2.toBuilder()
                .replyTo(firstComment.id())
                .build();
        final var secondComment = restTemplate.postForEntity(url, replyTo, Comment.class).getBody();

        // Add a third separate comment
        final var thirdComment = restTemplate.postForEntity(url, NEW_COMMENT_3, Comment.class).getBody();

        final var getResponse = restTemplate.getForEntity(url, Comment[].class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        final var comments = getResponse.getBody();
        assertThat(comments)
                .hasSize(2)
                .allSatisfy(comment -> {
                    assertThat(comment.id()).isNotEqualTo(secondComment.id());
                })
                .anySatisfy(comment -> {
                    assertThat(comment.id()).isEqualTo(thirdComment.id());
                })
                .anySatisfy(comment -> {
                    assertThat(comment.id()).isEqualTo(firstComment.id());
                    assertThat(comment.replies())
                            .hasSize(1)
                            .containsExactly(secondComment);
                });

    }

    /**
     * Validate that GET requests to /posts/{id}/comments
     * only returns published comments
     */
    @Test
    void testGetCommentsOnlyPublished() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var firstComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        // Add reply to first comment
        final var replyTo = NEW_COMMENT_2.toBuilder()
                .replyTo(firstComment.id())
                .build();
        final var secondComment = restTemplate.postForEntity(url, replyTo, Comment.class).getBody();

        jdbcTemplate.update("UPDATE comments SET published = false WHERE id = :id", Map.of("id", secondComment.id()));

        final var getResponse = restTemplate.getForEntity(url, Comment[].class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        final var comments = getResponse.getBody();
        assertThat(comments)
                .hasSize(1)
                .containsExactly(firstComment)
                .allSatisfy(comment -> {
                    assertThat(comment.replies()).isEmpty();
                });

        assertThat(countComments()).isEqualTo(2);
    }

    /**
     * Validate PUT requests to /posts/{id}/comments/{commentId}
     * update the comment content and last_edited date but not
     * reply_to
     */
    @Test
    void testUpdateComment() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var updatedComment = NEW_COMMENT_1.toBuilder()
                .content("An update of utmost importance")
                .replyTo(500L)
                .build();

        final var updatRequest = new HttpEntity<>(updatedComment);
        final var putResponse = restTemplate.exchange(url + "/" + originalComment.id(), PUT, updatRequest, Void.class);
        assertThat(putResponse.getStatusCode()).isEqualTo(NO_CONTENT);
        assertThat(countComments()).isEqualTo(1);

        final var getResponse = restTemplate.getForEntity(url, Comment[].class).getBody()[0];

        assertThat(getResponse.id()).isEqualTo(originalComment.id());
        assertThat(getResponse.content()).isEqualTo(updatedComment.content());
        assertThat(getResponse.replyTo()).isEqualTo(originalComment.replyTo());
        assertThat(getResponse.lastEdited()).isAfter(getResponse.postedDate());
    }

    /**
     * Validate that PUT requests to /posts/{id}/comments/{commentId}
     * return NOT_FOUND if the comment does not exist
     */
    @Test
    void testUpdateCommentNotFound() {
        final var posts = loadPosts(NEW_POST_1, NEW_POST_2);

        final var url = makeUrl("/posts/" + posts.get(0).id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var updatedComment = NEW_COMMENT_1.toBuilder()
                .content("An update of utmost importance")
                .build();
        final var commentNotOnPost = restTemplate.exchange(url + "/9000", PUT, new HttpEntity<>(updatedComment),
                String.class);
        assertThat(commentNotOnPost.getStatusCode()).isEqualTo(NOT_FOUND);
        assertThat(commentNotOnPost.getBody()).contains("Error ID",
                "Comment with ID %s for post %s not found".formatted(9000, posts.get(0).id()));

        final var retreivedComment = restTemplate.getForEntity(url, Comment[].class).getBody()[0];
        assertThat(retreivedComment).isEqualTo(originalComment);
    }

    /**
     * Validate that PUT requests to /posts/{id}/comments/{commentId}
     * return NOT_FOUND if the comment is for a different post
     */
    @Test
    void testUpdateCommentWrongPost() {
        final var posts = loadPosts(NEW_POST_1, NEW_POST_2);
        final var url = makeUrl("/posts/" + posts.get(0).id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var updatedComment = NEW_COMMENT_1.toBuilder()
                .content("An update of utmost importance")
                .build();
        final var otherPost = makeUrl("/posts/" + posts.get(1).id() + "/comments");
        final var commentNotOnPost = restTemplate.exchange(otherPost + "/" + originalComment.id(), PUT,
                new HttpEntity<>(updatedComment), String.class);

        validateErrorResponse(commentNotOnPost, NOT_FOUND,
                "Comment with ID %s for post %s not found".formatted(originalComment.id(), posts.get(1).id()));

        final var firstPostComments = restTemplate.getForEntity(url, Comment[].class).getBody();
        assertThat(firstPostComments)
                .hasSize(1)
                .containsExactly(originalComment);

        final var secondPostComments = restTemplate.getForEntity(otherPost, Comment[].class).getBody();
        assertThat(secondPostComments).isEmpty();
    }

    /**
     * Validate that PUT requests to /posts/{id}/comments/{commentId}
     * return BAD_REQUEST if the content is empty and does not
     * update the comment
     */
    @ParameterizedTest(name = "testUpdateCommentEmptyContent - Content: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    void testUpdateCommentEmptyContent(final String content) {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var emptyContentRequest = NEW_COMMENT_1.toBuilder()
                .content(content)
                .build();
        final var updateResponse = restTemplate.exchange(url + "/" + originalComment.id(), PUT,
                new HttpEntity<>(emptyContentRequest), String.class);

        validateErrorResponse(updateResponse, BAD_REQUEST, "Comment content cannot be empty");

        final var retreivedComment = restTemplate.getForEntity(url, Comment[].class).getBody()[0];
        assertThat(retreivedComment).isEqualTo(originalComment);
    }

    /**
     * Validate that PUT requests to /posts/{id}/comments/{commentId}
     * return BAD_REQUEST if the author is empty or is not the original
     * comment authorand does not update the comment
     */
    @ParameterizedTest(name = "testUpdateCommentEmptyAuthor - Author: \"{0}\"")
    @MethodSource("com.ncitguys.poster.utils.TestUtils#emptyStrings")
    @ValueSource(strings = { "MarkTwain" })
    void testUpdateCommentEmptyAuthor(final String author) {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var emptyAuthorRequest = NEW_COMMENT_1.toBuilder()
                .author(author)
                .build();
        final var updateResponse = restTemplate.exchange(url + "/" + originalComment.id(), PUT,
                new HttpEntity<>(emptyAuthorRequest), String.class);

        validateErrorResponse(updateResponse, BAD_REQUEST,
                "User %s is not the author of comment %s".formatted(author, originalComment.id()));
    }

    /**
     * Validate that DELETE requests to /posts/{id}/comments/{commentId}
     * unpublish the comment but does not delete it from the DB
     */
    @Test
    void testUnpublishComment() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");
        final var originalComment = restTemplate.postForEntity(url, NEW_COMMENT_1, Comment.class).getBody();

        final var deleteResponse = restTemplate.exchange(url + "/" + originalComment.id(), DELETE,
                HttpEntity.EMPTY, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(OK);
        assertThat(countComments()).isEqualTo(1);

        var getResponse = restTemplate.getForEntity(url, Comment[].class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody()).isEmpty();

        jdbcTemplate.update("UPDATE comments SET published = true WHERE id = :id", Map.of("id", originalComment.id()));

        getResponse = restTemplate.getForEntity(url, Comment[].class);
        assertThat(getResponse.getStatusCode()).isEqualTo(OK);
        assertThat(getResponse.getBody()).containsExactly(originalComment);
    }

    /**
     * Validate that DELETE requests to /posts/{id}/comments/{commentId}
     * return NOT_FOUND if the comment is not found
     */
    @Test
    void testUnpublishCommentNotFound() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var url = makeUrl("/posts/" + post.id() + "/comments");

        final var deleteResponse = restTemplate.exchange(url + "/9000", DELETE,
                HttpEntity.EMPTY, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    /**
     * Validate that DELETE requests to /posts/{id}/comments/{commentId}
     * return NOT_FOUND if the post is not found
     */
    @Test
    void testUnpublishCommentPostNotFound() {
        final var post = loadPosts(NEW_POST_1).getFirst();
        final var postUrl = makeUrl("/posts/" + post.id() + "/comments");
        final var comment = restTemplate.postForEntity(postUrl, NEW_COMMENT_1, Comment.class).getBody();

        final var nonExistentPostUrl = makeUrl("/posts/9000/comments/" + comment.id());
        final var deleteResponse = restTemplate.exchange(nonExistentPostUrl, DELETE,
                HttpEntity.EMPTY, Void.class);

        assertThat(deleteResponse.getStatusCode()).isEqualTo(NOT_FOUND);
    }

    /**
     * Validate all endpoints return a 500 response
     * if there is a database problem or other unexpected error
     */
    @ParameterizedTest(name = "urlsFailIfDatabaseFails - Path: \"{0}\", Method: \"{1}\"")
    @CsvSource(textBlock = """
            # PATH, METHOD BODY
            /posts, POST, {"author":"broken", "content":"broken"}
            /posts/author/MarkTwain, GET, null
            /posts/9000, GET, null
            /posts/9000, DELETE, null
            /posts/9000, PUT, {"author":"broken", "content":"broken"}
            /posts/9000/comments, POST, {"replyTo": null,"author":"broken", "content":"broken"}
            /posts/9000/comments/9000, GET, null
            /posts/9000/comments/9000, PUT, {"author":"broken", "content":"broken"}
            /posts/9000/comments/9000, DELETE, null
            """)
    void urlsFailIfDatabaseFails(final String path, final String method, final String body) {
        jdbcTemplate.update("ALTER TABLE posts RENAME TO missing_posts", Map.of());

        final var url = makeUrl(path);
        final ResponseEntity<String> response = switch (method) {
            case "POST" -> restTemplate.postForEntity(url, body, String.class);
            case "GET" -> restTemplate.getForEntity(url, String.class);
            case "PUT" -> restTemplate.exchange(url, PUT, new HttpEntity<>(body), String.class);
            case "DELETE" -> restTemplate.exchange(url, DELETE, HttpEntity.EMPTY, String.class);
            default -> throw new IllegalArgumentException("Unsupported method: " + method);

        };

        validateErrorResponse(response, INTERNAL_SERVER_ERROR, "Unexpected Error");

        jdbcTemplate.update("ALTER TABLE missing_posts RENAME TO posts", Map.of());
    }

    /**
     * Convenience method for crafting the URL for a test
     */
    private String makeUrl(final String path) {
        return "http://localhost:" + port + path;
    }

    /**
     * Convenience method for getting the current post count
     * in the database
     *
     * @return the count of posts in the database
     */
    private int countPosts() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM posts", Map.of(), Integer.class);
    }

    /**
     * Convenience method for getting the current comment count
     * in the database
     *
     * @return the count of comments in the database
     */
    private int countComments() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM comments", Map.of(), Integer.class);
    }

    /**
     * Convenience method for loading posts to the REST API
     *
     * @param posts the posts to load
     * @return the list of loaded posts
     */
    private List<Post> loadPosts(final NewPost... posts) {
        final var url = makeUrl("/posts");
        return TestUtils.loadPosts(restTemplate, url, posts);
    }

}
