package com.ncitguys.poster.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.db.repository.CommentRepository;
import com.ncitguys.poster.db.repository.PostRepository;

@ExtendWith(MockitoExtension.class)
public class AdminControllerTest {

    @Mock
    private PostRepository postRepo;

    @Mock
    private CommentRepository commentRepo;

    @InjectMocks
    private AdminController adminController;

    /**
     * Validate the delete comment method is
     * calling the right repository methods
     */
    @Test
    void testDeleteComment() {
        final var commentId = 1L;
        assertDoesNotThrow(() -> adminController.deleteComment(commentId));
        verify(commentRepo).deleteById(commentId);
    }

    /**
     * Validate the delete post method is
     * calling the right repository methods
     */
    @Test
    void testDeletePost() {
        final var postId = 1L;
        assertDoesNotThrow(() -> adminController.deletePost(postId));
        verify(postRepo).deleteById(postId);
        verify(commentRepo).deleteByPost(postId);
    }

    /**
     * Validate the get all posts method is
     * returning the posts that are returned by
     * the repository
     */
    @Test
    void testGetAllPosts() {
        final var now = OffsetDateTime.now();
        final var postsList = List.of(
                new Post(1L, now, now, "author", "content", true),
                new Post(2L, now, now, "author", "content", true));

        when(postRepo.getAllOrderByPostedDateDesc()).thenReturn(postsList);
        final var results = adminController.getAllPosts();
        assertThat(results)
                .isNotNull()
                .containsExactlyInAnyOrderElementsOf(postsList);
    }
}
