package com.ncitguys.poster.controller;

import static com.ncitguys.poster.utils.IntegrationData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.db.entity.Comment;
import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.db.repository.CommentRepository;
import com.ncitguys.poster.utils.TestUtils;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Timeout(30)
public class AdminControllerIntegrationTest {

    @TestConfiguration
    static class AuthenticatedTemplateConfig {

        private final String userName = "admin";;

        private final String password = "super-secret";

        @Bean
        public RestTemplateBuilder restTemplateBuilder() {
            return new RestTemplateBuilder().basicAuthentication(userName, password);
        }
    }

    @LocalServerPort
    private int port;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    private ObjectMapper jsonMapper;

    @BeforeEach
    void initDb() {
        jdbcTemplate.update("TRUNCATE posts CASCADE; TRUNCATE comments CASCADE;", Map.of());
    }

    /**
     * Validate that a GET request to /admin/posts
     * returns a 200 response with the list of posts
     */
    @Test
    public void getAllPostsWorks() {
        final var posts = loadPosts(NEW_POST_1, NEW_POST_2, NEW_POST_3, NEW_POST_4);
        final var response = restTemplate.getForEntity(makeUrl("/admin/posts"), Post[].class);
        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody())
                .hasSize(4)
                .containsExactlyInAnyOrderElementsOf(posts);
    }

    /**
     * Validate that a DELETE request to /admin/posts/{id} deletes
     * the post and associated comments
     */
    @Test
    public void deletePostWorks() {
        final var posts = loadPosts(NEW_POST_1, NEW_POST_2);
        final var post1CommentUrl = makeUrl("/posts/" + posts.get(0).id() + "/comments");
        final var post2CommentUrl = makeUrl("/posts/" + posts.get(1).id() + "/comments");
        final var comment1 = restTemplate.postForEntity(post1CommentUrl, NEW_COMMENT_1, Comment.class).getBody();
        restTemplate.postForEntity(post1CommentUrl,
                NEW_COMMENT_2.toBuilder().replyTo(comment1.id()).build(), Comment.class).getBody();
        final var comment3 = restTemplate.postForEntity(post2CommentUrl, NEW_COMMENT_3, Comment.class).getBody();

        assertThat(countComments()).isEqualTo(3);

        final var deleteUrl = makeUrl("/admin/posts/" + posts.get(0).id());
        final var response = restTemplate.exchange(deleteUrl, DELETE, HttpEntity.EMPTY, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(OK);

        assertThat(countComments()).isEqualTo(1);
        assertThat(commentRepository.findByPostId(posts.get(0).id())).isEmpty();
        assertThat(commentRepository.findByPostId(posts.get(1).id()))
                .hasSize(1)
                .containsOnly(comment3);
    }

    /**
     * Validate that a DELETE request to /admin/posts/{id}
     * returns OK even if the post does not exist
     */
    @Test
    public void deletePostSucceedsIfNotFound() {
        final var url = makeUrl("/admin/posts/9000");
        final var response = restTemplate.exchange(url, DELETE, HttpEntity.EMPTY, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(OK);
    }

    /**
     * Validate that a DELETE request to /admin/comments/{id}
     * deletes the comment and sets its children's `reply_to` field to null
     */
    @Test
    public void deleteCommentWorks() {
        final var post = loadPosts(NEW_POST_1).get(0);
        final var commentUrl = makeUrl("/posts/" + post.id() + "/comments");
        final var comment1 = restTemplate.postForEntity(commentUrl, NEW_COMMENT_1, Comment.class).getBody();
        final var comment2 = restTemplate.postForEntity(commentUrl,
                NEW_COMMENT_2.toBuilder().replyTo(comment1.id()).build(), Comment.class).getBody();
        final var comment3 = restTemplate.postForEntity(commentUrl,
                NEW_COMMENT_3.toBuilder().replyTo(comment1.id()).build(), Comment.class).getBody();
        final var comment4 = restTemplate.postForEntity(commentUrl,
                NEW_COMMENT_4.toBuilder().replyTo(comment3.id()).build(), Comment.class).getBody();

        assertThat(countComments()).isEqualTo(4);

        var allComments = commentRepository.findByPostId(post.id());
        assertThat(allComments)
                .hasSize(1)
                .allSatisfy(comment -> {
                    assertThat(comment.replyTo()).isNull();
                    assertThat(comment.id()).isEqualTo(comment1.id());
                    assertThat(comment.replies())
                            .hasSize(2)
                            .anySatisfy(child -> {
                                assertThat(child.id()).isEqualTo(comment2.id());
                                assertThat(child.replyTo()).isEqualTo(comment1.id());
                            })
                            .anySatisfy(child -> {
                                assertThat(child.id()).isEqualTo(comment3.id());
                                assertThat(child.replyTo()).isEqualTo(comment1.id());
                                assertThat(child.replies())
                                        .hasSize(1)
                                        .allSatisfy(grandchild -> {
                                            assertThat(grandchild.id()).isEqualTo(comment4.id());
                                            assertThat(grandchild.replyTo()).isEqualTo(comment3.id());
                                        });
                            });
                });
        final var deleteUrl = makeUrl("/admin/comments/" + comment1.id());
        final var response = restTemplate.exchange(deleteUrl, DELETE, HttpEntity.EMPTY, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(OK);

        assertThat(countComments()).isEqualTo(3);

        allComments = commentRepository.findByPostId(post.id());
        assertThat(allComments)
                .hasSize(2)
                .anySatisfy(comment -> {
                    assertThat(comment.replyTo()).isNull();
                    assertThat(comment.id()).isEqualTo(comment2.id());
                    assertThat(comment.replies()).isEmpty();
                })
                .anySatisfy(comment -> {
                    assertThat(comment.replyTo()).isNull();
                    assertThat(comment.id()).isEqualTo(comment3.id());
                    assertThat(comment.replies())
                            .hasSize(1)
                            .anySatisfy(child -> {
                                assertThat(child.id()).isEqualTo(comment4.id());
                                assertThat(child.replyTo()).isEqualTo(comment3.id());
                            });
                });
    }

    /**
     * Convenience method for crafting the URL for a test
     *
     * @param path the path of the URL
     * @return the full URL
     */
    private String makeUrl(final String path) {
        return "http://localhost:" + port + path;
    }

    /**
     * Convenience method for loading posts to the REST API
     *
     * @param posts the posts to load
     * @return the list of loaded posts
     */
    private List<Post> loadPosts(final NewPost... posts) {
        final var url = makeUrl("/posts");
        return TestUtils.loadPosts(restTemplate, url, posts);
    }

    /**
     * Count the number of comments in the database
     *
     * @return the number of comments
     */
    private int countComments() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM comments", Map.of(), Integer.class);
    }
}
