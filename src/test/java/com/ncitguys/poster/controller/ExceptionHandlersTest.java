package com.ncitguys.poster.controller;

import static com.ncitguys.poster.utils.TestUtils.validateErrorResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.postgresql.util.PSQLException;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.server.ResponseStatusException;

public class ExceptionHandlersTest {

    public static final Pattern UUID = Pattern
            .compile("[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}");

    public static final ExceptionHandlers handler = new ExceptionHandlers();

    /**
     * Validate that the handleDbException method is
     * generating error ids and logging them as well
     * as returning it to the use.
     *
     * @param output captured logging output
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleDbExceptionHandlesGeneralPSQLException(final CapturedOutput output) {
        final var error = new PSQLException("big-bada-boom", null);

        // Validate the response
        final var response = handler.handleDbException(error);
        validateErrorResponse(response, INTERNAL_SERVER_ERROR, "Database error");

        // Validate the log
        assertThat(output).contains("big-bada-boom");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();

        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

    /**
     * Validate that the handleDbException method is handling
     * the case when we try to add a reply to a comment that isn't
     * for the same post we're currently commenting on.
     *
     * @param output captured logging output
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleDbExceptionHandlesDataIntegrityViolationExceptionForReplyTo(final CapturedOutput output) {
        final var dive = new DataIntegrityViolationException(
                "test exception violates foreign key constraint \"comments_comment_on_reply_to_fkey\"");

        // Validate the response
        final var response = handler.handleDbException(dive);
        validateErrorResponse(response, BAD_REQUEST, "replyTo value is not a comment on this post");

        // Validate the log
        assertThat(output).contains(
                "test exception violates foreign key constraint \"comments_comment_on_reply_to_fkey\"");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();
        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

    /**
     * Validate that the handleDbException method is handling
     * the case when we try to add a comment to a post that
     * doesn't exist.
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleDbExceptionHandlesDataIntegrityViolationExceptionForCommentTo(final CapturedOutput output) {
        final var dive = new DataIntegrityViolationException(
                "test exception violates foreign key constraint \"comments_comment_on_fkey\"");

        // Validate the response
        final var response = handler.handleDbException(dive);

        validateErrorResponse(response, NOT_FOUND, "Cannot add comment to non-existent post");

        // Validate the log
        assertThat(output).contains(
                "test exception violates foreign key constraint \"comments_comment_on_fkey\"");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();

        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

    /**
     * Validate that the handledDbException method handles other
     * data integrity exceptions as any other DB exception
     *
     * @param output captured logging output
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleDbExceptionHandlesOtherExceptions(final CapturedOutput output) {
        final var dive = new DataIntegrityViolationException("big-bada-boom");

        // Validate the response
        final var response = handler.handleDbException(dive);

        validateErrorResponse(response, INTERNAL_SERVER_ERROR, "Database error");

        // Validate the log
        assertThat(output).contains("big-bada-boom");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();

        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

    /**
     * Validte that the handleResponseStatusException method
     * handles is reporting the status and reason from the
     * exception back to the user
     *
     * @param output captured logging output
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleResponseStatusExceptionHandlesResponseStatusException(final CapturedOutput output) {
        final var error = new ResponseStatusException(FORBIDDEN, "big-bada-boom");

        // Validate the response
        final var response = handler.handleResponseStatusException(error);

        validateErrorResponse(response, FORBIDDEN, "big-bada-boom");

        // Validate the log
        assertThat(output).contains("big-bada-boom");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();

        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

    /**
     * Validate that the handleAllOtherExceptions method
     * handles generates an error ID, logs it and returns
     * that to the user.
     *
     * @param output captured logging output
     */
    @Test
    @ExtendWith(OutputCaptureExtension.class)
    void handleAllOtherExceptionsHandlesOtherExceptions(final CapturedOutput output) {
        final var error = new RuntimeException("big-bada-boom");

        // Validate the response
        final var response = handler.handleAllOtherExceptions(error);
        validateErrorResponse(response, INTERNAL_SERVER_ERROR, "Unexpected Error");

        // Validate the log
        assertThat(output).contains("big-bada-boom");
        final var logMatcher = UUID.matcher(output.getOut());
        assertThat(logMatcher.find()).isTrue();

        final var bodyMatcher = UUID.matcher(response.getBody());
        bodyMatcher.find();

        // Validate the log and response have the same error id
        assertThat(bodyMatcher.group()).isEqualTo(logMatcher.group());
    }

}
