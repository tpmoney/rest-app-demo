package com.ncitguys.poster.controller;

import static java.time.ZoneOffset.UTC;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.time.Clock;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.server.ResponseStatusException;

import com.ncitguys.poster.api.NewComment;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.db.entity.Comment;
import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.db.repository.CommentRepository;
import com.ncitguys.poster.db.repository.PostRepository;

@ExtendWith(MockitoExtension.class)
public class PostControllerTest {

    private static final OffsetDateTime NOW = OffsetDateTime.now();
    private static final OffsetDateTime NOW_PLUS_5 = NOW.plusHours(5);
    private static final Long POST_1_ID = 1L;
    private static final Long COMMENT_1_ID = 101L;
    private static final String AUTHOR = "test_author";

    private static final Post POST_1 = new Post(POST_1_ID, NOW, NOW, AUTHOR, "content_1", true);
    private static final Post POST_2 = new Post(2L, NOW, NOW, AUTHOR, "content_2", true);

    private static final Comment COMMENT_1 = new Comment(
            COMMENT_1_ID, POST_1_ID, null, NOW, NOW, AUTHOR, "I agree", true, emptyList());
    private static final Comment COMMENT_2 = new Comment(
            202L, POST_1_ID, null, NOW, NOW, AUTHOR, "I disagree", true, emptyList());

    private static final NewPost POST_UPDATE_REQUEST = new NewPost(AUTHOR, "I've changed my content");
    private static final NewComment COMMENT_UPDATE_REQUEST = new NewComment(null, AUTHOR, "I've changed my mind");

    private static Stream<String> emptyStrings() {
        return Stream.of("", " ", null);
    }

    @Mock
    private PostRepository postRepo;
    @Mock
    private CommentRepository commentRepo;
    @Mock
    Clock clock;
    @Mock
    Instant instant;

    @InjectMocks
    private PostController postController;

    /**
     * Validate that getByAuthor returns the list of posts
     * provided by the repository.
     */
    @Test
    void testGetByAuthor() {
        when(postRepo.findByAuthorAndPublishedIsTrueOrderByPostedDateDesc(AUTHOR))
                .thenReturn(List.of(POST_1, POST_2));

        final var result = postController.getByAuthor(AUTHOR);
        assertThat(result)
                .hasSize(2)
                .containsExactly(POST_1, POST_2);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that getByAuthor doesn't intercept exceptions
     * from the post repository.
     */
    @Test
    void testGetByAuthorError() {
        when(postRepo.findByAuthorAndPublishedIsTrueOrderByPostedDateDesc(AUTHOR))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.getByAuthor(AUTHOR))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");
    }

    /**
     * Validate that getComments returns the list of comments
     * provided by the repository.
     */
    @Test
    void testGetComments() {
        when(commentRepo.findByPostId(POST_1_ID))
                .thenReturn(List.of(COMMENT_1, COMMENT_2));

        final var result = postController.getComments(POST_1_ID);
        assertThat(result)
                .hasSize(2)
                .containsExactly(COMMENT_1, COMMENT_2);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that getComments doesn't intercept exceptions
     * from the comment repository.
     */
    @Test
    void testGetCommentsError() {
        when(commentRepo.findByPostId(POST_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.getComments(POST_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");
    }

    /**
     * Validate that getPost returns the post provided by the repository.
     */
    @Test
    void testGetPost() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));

        final var result = postController.getPost(POST_1_ID);
        assertThat(result)
                .isEqualTo(POST_1);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that getPost throws a NOT_FOUND ResponseStatusException
     * if the post is not returned by the repository.
     */
    @Test
    void testGetPostNotFound() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.empty());
        final var expectedErrorMessage = "Post with ID %s not found".formatted(POST_1_ID);

        assertThatThrownBy(() -> postController.getPost(POST_1_ID))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(NOT_FOUND);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that getPost doesn't intercept exceptions
     * from the post repository.
     */
    @Test
    void testGetPostError() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.getPost(POST_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");
    }

    /**
     * Validate that unpublishComment:
     * <p/>
     * <ul>
     * <li>removes the comment from the repository</li>
     * <li>updates the published field to false</li>
     * <li>saves the updated comment</li>
     * </ul>
     */
    @Test
    void testUnpublishComment() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.of(COMMENT_1));
        final var modifiedComment = COMMENT_1.toBuilder()
                .published(false)
                .build();

        postController.unpublishComment(POST_1_ID, COMMENT_1_ID);
        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verify(commentRepo).save(modifiedComment);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that unpublishComment throws if the comment isn't found
     * and doesn't try to update the comment in the repository.
     */
    @Test
    void testUnpublishCommentNotFound() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.empty());
        final var expectedErrorMessage = "Comment with ID %s for post %s not found".formatted(COMMENT_1_ID, POST_1_ID);
        assertThatThrownBy(() -> postController.unpublishComment(POST_1_ID, COMMENT_1_ID))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(NOT_FOUND);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that unpublishComment doesn't intercept exceptions from
     * the comment repository when searching for the comment.
     */
    @Test
    void testUnpublishCommentError() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.unpublishComment(POST_1_ID, COMMENT_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that unpublishComment doesn't intercept exceptions from
     * the comment repository when trying to save the comment.
     */
    @Test
    void testUnpublishCommentErrorSaving() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.of(COMMENT_1));
        when(commentRepo.save(any(Comment.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.unpublishComment(POST_1_ID, COMMENT_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verify(commentRepo).save(any(Comment.class));
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that unpublishPost
     * <p/>
     * <ul>
     * <li>searches for the post in the repository</li>
     * <li>updates the published field to false</li>
     * <li>saves the updated post</li>
     * </ul>
     */
    @Test
    void testUnpublishPost() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));
        final var modifiedPost = POST_1.toBuilder()
                .published(false)
                .build();

        postController.unpublishPost(POST_1_ID);
        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verify(postRepo).save(modifiedPost);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that unpublishPost throws if the post isn't found
     * and doesn't try to update the post in the repository.
     */
    @Test
    void testUnpublishPostNotFound() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.empty());
        final var expectedErrorMessage = "Post with ID %s not found".formatted(POST_1_ID);
        assertThatThrownBy(() -> postController.unpublishPost(POST_1_ID))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(NOT_FOUND);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that unpublishPost doesn't intercept exceptions from
     * the post repository when searching for the post.
     */
    @Test
    void testUnpublishPostError() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.unpublishPost(POST_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that unpublishPost doesn't intercept exceptions from
     * the post repository when trying to save the post.
     */
    @Test
    void testUnpublishPostErrorSaving() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));
        when(postRepo.save(any(Post.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.unpublishPost(POST_1_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verify(postRepo).save(any(Post.class));
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updateComment
     * <p/>
     * <ul>
     * <li>searches for the comment in the repository</li>
     * <li>updates the comment's content and edit date</li>
     * <li>saves the updated comment</li>
     * <li>returns a NO_CONTENT response</li>
     * </ul>
     */
    @Test
    void testUpdateComment() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.of(COMMENT_1));
        when(clock.instant()).thenReturn(instant);
        when(instant.atOffset(UTC)).thenReturn(NOW_PLUS_5);
        final var updatedComment = COMMENT_1.toBuilder()
                .content(COMMENT_UPDATE_REQUEST.content())
                .lastEdited(NOW_PLUS_5)
                .build();

        final var response = postController.updateComment(POST_1_ID, COMMENT_1_ID, COMMENT_UPDATE_REQUEST);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(NO_CONTENT);

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verify(clock).instant();
        verify(commentRepo).save(updatedComment);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate the updateComment throws if the new content is empty.
     */
    @ParameterizedTest(name = "testUpdateCommentEmptyContent - Content: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUpdateCommentEmptyContent(final String content) {
        final var expectedErrorMessage = "Comment content cannot be empty";
        final var emptyContentRequest = COMMENT_UPDATE_REQUEST.toBuilder()
                .content(content)
                .build();
        assertThatThrownBy(() -> postController.updateComment(POST_1_ID, COMMENT_1_ID, emptyContentRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(commentRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that updateComment throws if the comment isn't found
     * and doesn't try to update the comment in the repository.
     */
    @Test
    void testUpdateCommentNotFound() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.empty());
        final var expectedErrorMessage = "Comment with ID %s for post %s not found".formatted(COMMENT_1_ID, POST_1_ID);
        assertThatThrownBy(() -> postController.updateComment(POST_1_ID, COMMENT_1_ID, COMMENT_UPDATE_REQUEST))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(NOT_FOUND);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that updateComment throws if the author of the new comment isn't
     * the same as the original comment author and does not attempt to update the
     * comment in the repository.
     */
    @Test
    void testUpdateCommentAuthorMismatch() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.of(COMMENT_1));
        final var mismatchedRequest = COMMENT_UPDATE_REQUEST.toBuilder()
                .author("An-Imposter")
                .build();
        final var expectedErrorMessage = "User %s is not the author of comment %s"
                .formatted(mismatchedRequest.author(), COMMENT_1_ID);
        assertThatThrownBy(() -> postController.updateComment(POST_1_ID, COMMENT_1_ID, mismatchedRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that updateComment doesn't intercept exceptions from the
     * comment repository when searching for the comment.
     */
    @Test
    void testUpdateCommentError() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.updateComment(POST_1_ID, COMMENT_1_ID, COMMENT_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that updateComment doesn't intercept exceptions from the
     * comment repository when saving the updated comment.
     */
    @Test
    void testUpdateCommentErrorSaving() {
        when(commentRepo.findByPostAndCommentId(POST_1_ID, COMMENT_1_ID))
                .thenReturn(Optional.of(COMMENT_1));
        when(clock.instant()).thenReturn(instant);
        when(instant.atOffset(UTC)).thenReturn(NOW_PLUS_5);
        when(commentRepo.save(any(Comment.class))).thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.updateComment(POST_1_ID, COMMENT_1_ID, COMMENT_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(commentRepo).findByPostAndCommentId(POST_1_ID, COMMENT_1_ID);
        verify(commentRepo).save(any(Comment.class));
        verifyNoMoreInteractions(commentRepo);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that updatePost
     * <p/>
     * <ul>
     * <li>Searches for the post in the repository</li>
     * <li>Updates the post</li>
     * <li>Saves the updated post</li>
     * <li>Returns a NO_CONTENT response</li>
     * </ul>
     */
    @Test
    void testUpdatePost() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));
        when(clock.instant()).thenReturn(instant);
        when(instant.atOffset(UTC)).thenReturn(NOW_PLUS_5);
        final var updatedPost = POST_1.toBuilder()
                .content(POST_UPDATE_REQUEST.content())
                .lastEdited(NOW_PLUS_5)
                .build();

        final var result = postController.updatePost(POST_1_ID, POST_UPDATE_REQUEST);
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(NO_CONTENT);

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verify(postRepo).save(updatedPost);
        verify(clock).instant();
        verify(instant).atOffset(UTC);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updatePost throws when the new content is empty
     * and doesn't try to update the post.
     */
    @ParameterizedTest(name = "testUpdatePostEmptyContent - Content: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUpdatePostEmptyContent(final String content) {
        final var expectedErrorMessage = "Post content cannot be empty";
        final var missingContentRequest = POST_UPDATE_REQUEST.toBuilder()
                .content(content)
                .build();
        assertThatThrownBy(
                () -> postController.updatePost(POST_1_ID, missingContentRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updatePost throws when the post isn't found
     * in the repository and doesn't try to update the post.
     */
    @Test
    void testUpdatePostNotFound() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.empty());

        final var expectedErrorMessage = "Post with ID %s not found".formatted(POST_1_ID);
        assertThatThrownBy(() -> postController.updatePost(POST_1_ID, POST_UPDATE_REQUEST))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(NOT_FOUND);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updatePost throws when the author of the updated
     * post doesn't match the author of the original post and doesn't
     * try to update the post.
     */
    @Test
    void testUpdatePostAuthorMismatch() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));
        final var mismatchedRequest = POST_UPDATE_REQUEST.toBuilder()
                .author("An-Imposter")
                .build();
        final var expectedErrorMessage = "User %s is not the author of post %s".formatted(
                mismatchedRequest.author(), POST_1_ID);

        assertThatThrownBy(() -> postController.updatePost(POST_1_ID, mismatchedRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updatePost doesn't intercept exceptions from the
     * post repository when searching for the post.
     */
    @Test
    void testUpdatePostError() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.updatePost(POST_1_ID, POST_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that updatePost doesn't intercept exceptions from the
     * post repository when saving the post
     */
    @Test
    void testUpdatePostErrorSaving() {
        when(postRepo.findByIdAndPublishedIsTrue(POST_1_ID))
                .thenReturn(Optional.of(POST_1));
        when(clock.instant()).thenReturn(instant);
        when(postRepo.save(any(Post.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.updatePost(POST_1_ID, POST_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(postRepo).findByIdAndPublishedIsTrue(POST_1_ID);
        verify(postRepo).save(any(Post.class));
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadComment saves the new copmment
     * and returns a CREATED response with the new comment
     * and a location header with the URI of the new comment
     */
    @Test
    void testUploadComment() {
        when(commentRepo.save(any(Comment.class))).thenReturn(COMMENT_1);
        when(clock.instant()).thenReturn(instant);
        when(instant.atOffset(UTC)).thenReturn(NOW);

        final var response = postController.uploadComment(POST_1_ID, COMMENT_UPDATE_REQUEST);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(CREATED);
        assertThat(response.getHeaders().getLocation()).isNotNull();
        assertThat(response.getHeaders().getLocation().toString()).isEqualTo("./" + COMMENT_1_ID);

        final var commentCaptor = ArgumentCaptor.forClass(Comment.class);
        verify(commentRepo).save(commentCaptor.capture());

        final var capturedComment = commentCaptor.getValue();
        assertThat(capturedComment.author()).isEqualTo(COMMENT_UPDATE_REQUEST.author());
        assertThat(capturedComment.content()).isEqualTo(COMMENT_UPDATE_REQUEST.content());
        assertThat(capturedComment.commentOn()).isEqualTo(POST_1_ID);
        assertThat(capturedComment.replyTo()).isEqualTo(COMMENT_UPDATE_REQUEST.replyTo());
        assertThat(capturedComment.published()).isTrue();
        assertThat(capturedComment.postedDate()).isEqualTo(NOW);
        assertThat(capturedComment.lastEdited()).isEqualTo(NOW);

        verify(clock).instant();
        verify(instant).atOffset(UTC);
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that uploadComment throws when the content is empty
     * and doesn't try to save the comment
     */
    @ParameterizedTest(name = "testUploadCommentEmptyContent - Content: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUploadCommentEmptyContent(final String content) {
        final var expectedErrorMessage = "Comment content cannot be empty";
        final var missingContentRequest = COMMENT_UPDATE_REQUEST.toBuilder()
                .content(content)
                .build();
        assertThatThrownBy(
                () -> postController.uploadComment(POST_1_ID, missingContentRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadComment throws when the author is empty
     * and doesn't try to save the comment
     */
    @ParameterizedTest(name = "testUploadCommentEmptyAuthor - Author: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUploadCommentEmptyAuthor(final String author) {
        final var expectedErrorMessage = "Comment author cannot be empty";
        final var missingAuthorRequest = COMMENT_UPDATE_REQUEST.toBuilder()
                .author(author)
                .build();
        assertThatThrownBy(
                () -> postController.uploadComment(POST_1_ID, missingAuthorRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadComment doesn't intercept exceptions from the
     * comment repository when saving the comment
     */
    @Test
    void testUploadCommentErrorSaving() {
        when(clock.instant()).thenReturn(instant);
        when(commentRepo.save(any(Comment.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.uploadComment(POST_1_ID, COMMENT_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(commentRepo).save(any(Comment.class));
        verifyNoMoreInteractions(commentRepo);
        verify(clock).instant();
        verifyNoInteractions(postRepo);
    }

    /**
     * Validate that uploadPost saves the new post
     * and returns a CREATED response with the new post
     * and a location header with the URI of the new post
     */
    @Test
    void testUploadPost() {
        when(postRepo.save(any(Post.class))).thenReturn(POST_1);
        when(clock.instant()).thenReturn(instant);
        when(instant.atOffset(UTC)).thenReturn(NOW);

        final var response = postController.uploadPost(POST_UPDATE_REQUEST);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(CREATED);
        assertThat(response.getHeaders().getLocation()).isNotNull();
        assertThat(response.getHeaders().getLocation().toString()).isEqualTo("./" + POST_1_ID);

        final var postCaptor = ArgumentCaptor.forClass(Post.class);
        verify(postRepo).save(postCaptor.capture());

        final var capturedPost = postCaptor.getValue();
        assertThat(capturedPost.author()).isEqualTo(POST_UPDATE_REQUEST.author());
        assertThat(capturedPost.content()).isEqualTo(POST_UPDATE_REQUEST.content());
        assertThat(capturedPost.published()).isTrue();
        assertThat(capturedPost.postedDate()).isEqualTo(NOW);
        assertThat(capturedPost.lastEdited()).isEqualTo(NOW);

        verify(clock).instant();
        verify(instant).atOffset(UTC);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadPost throws when the content is empty
     * and doesn't try to save the post
     */
    @ParameterizedTest(name = "testUploadPostEmptyContent - Content: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUploadPostEmptyContent(final String content) {
        final var expectedErrorMessage = "Post content cannot be empty";
        final var missingContentRequest = POST_UPDATE_REQUEST.toBuilder()
                .content(content)
                .build();
        assertThatThrownBy(
                () -> postController.uploadPost(missingContentRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadPost throws when the author is empty
     * and doesn't try to save the post
     */
    @ParameterizedTest(name = "testUploadPostEmptyAuthor - Author: \"{0}\"")
    @MethodSource("emptyStrings")
    void testUploadPostEmptyAuthor(final String author) {
        final var expectedErrorMessage = "Post author cannot be empty";
        final var missingAuthorRequest = POST_UPDATE_REQUEST.toBuilder()
                .author(author)
                .build();
        assertThatThrownBy(
                () -> postController.uploadPost(missingAuthorRequest))
                .isInstanceOfSatisfying(ResponseStatusException.class, ex -> {
                    assertThat(ex.getStatusCode()).isEqualTo(BAD_REQUEST);
                    assertThat(ex.getReason()).isEqualTo(expectedErrorMessage);
                });

        verifyNoInteractions(postRepo);
        verifyNoInteractions(clock);
        verifyNoInteractions(commentRepo);
    }

    /**
     * Validate that uploadPost doesn't intercept exceptions from the
     * post repository when saving the post
     */
    @Test
    void testUploadPostErrorSaving() {
        when(clock.instant()).thenReturn(instant);
        when(postRepo.save(any(Post.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> postController.uploadPost(POST_UPDATE_REQUEST))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("big-bada-boom");

        verify(postRepo).save(any(Post.class));
        verifyNoMoreInteractions(postRepo);
        verifyNoInteractions(commentRepo);
    }
}
