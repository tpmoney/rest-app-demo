package com.ncitguys.poster.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CREATED;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.config.GeneralConfig;
import com.ncitguys.poster.db.entity.Post;

public class TestUtils {
    public TestUtils() {
        // Utility class, no instantiation
    }

    public static final Pattern ERROR_ID_PATTERN = Pattern
            .compile("Error ID: [0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");

    public static ObjectMapper jsonMapper() {
        return new GeneralConfig().jsonMapper();
    }

    public static PrettyPrinter prettyPrinter() {
        final var printer = new DefaultPrettyPrinter();
        final var indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
        printer.indentObjectsWith(indenter);
        printer.indentArraysWith(indenter);
        return printer;
    }

    /**
     * Convenience method to validate the error responses from the REST API
     * <p/>
     * In addition to validating the status code and message list, this will
     * also valdiate that the message contains an error UUID
     *
     * @param body             the body to validate
     * @param expectedStatus   the expected HTTP status
     * @param expectedMessages strings that the body should contain
     */
    public static void validateErrorResponse(final ResponseEntity<String> response, final HttpStatus expectedStatus,
            final String... expectedMessages) {
        assertThat(response.getStatusCode()).isEqualTo(expectedStatus);
        assertThat(response.getBody())
                .isNotBlank()
                .contains(expectedMessages)
                .containsPattern(ERROR_ID_PATTERN);
    }

    public static Stream<String> emptyStrings() {
        return Stream.of("", " ", null);
    }

    /**
     * Convenience method for loading multiple posts into
     * the database for testing purposes
     *
     * @param posts the posts to load
     */
    public static List<Post> loadPosts(final TestRestTemplate restTemplate, final String url, final NewPost... posts) {
        final var results = new ArrayList<Post>();
        for (final var post : posts) {
            final var response = restTemplate.postForEntity(url, post, Post.class);
            assertThat(response.getStatusCode()).isEqualTo(CREATED);
            results.add(response.getBody());
        }

        return results;
    }
}
