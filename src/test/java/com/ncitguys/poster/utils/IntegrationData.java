package com.ncitguys.poster.utils;

import com.ncitguys.poster.api.NewComment;
import com.ncitguys.poster.api.NewPost;

/**
 * Various pre-made data objects for use in integration tests
 */
public class IntegrationData {

    public static final String AUTHOR_1 = "PostBot2000";
    public static final String AUTHOR_2 = "ElizaFan23";

    public static final NewPost NEW_POST_1 = new NewPost(AUTHOR_1, "New insights!");
    public static final NewPost NEW_POST_2 = new NewPost(AUTHOR_1, "Even more insights!");
    public static final NewPost NEW_POST_3 = new NewPost(AUTHOR_2, "Chat bots are cool!");
    public static final NewPost NEW_POST_4 = new NewPost(AUTHOR_2, "Chat bots the future!");

    public static final String COMMENT_AUTHOR_1 = "GPT-Genius";
    public static final String COMMENT_AUTHOR_2 = "SpamBot";

    public static final NewComment NEW_COMMENT_1 = new NewComment(null, COMMENT_AUTHOR_1, "I agree");
    public static final NewComment NEW_COMMENT_2 = new NewComment(null, COMMENT_AUTHOR_2, "I disagree");
    public static final NewComment NEW_COMMENT_3 = new NewComment(
            null, COMMENT_AUTHOR_1, "Spam bots hate this one weird trick!");
    public static final NewComment NEW_COMMENT_4 = new NewComment(null, COMMENT_AUTHOR_2, "I love that trick!");
}
