package com.ncitguys.poster.api;

import static com.ncitguys.poster.utils.TestUtils.jsonMapper;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NewPostTest {
    public static final String newPostJson = """
            {
                "author": "author",
                "content": "content"
            }
            """;

    public static final NewPost newPostRecord = new NewPost("author", "content");

    public static final ObjectMapper mapper = jsonMapper();

    @Test
    void recordDeserializesProperly() throws Exception {
        final var mappedPost = mapper.readValue(newPostJson, NewPost.class);
        assertThat(mappedPost).isEqualTo(newPostRecord);
    }
}
