package com.ncitguys.poster.api;

import static com.ncitguys.poster.utils.TestUtils.jsonMapper;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NewCommentTest {
    public static final String newCommentJson = """
            {
                "replyTo": 2,
                "author": "author",
                "content": "content"
            }
            """;

    public static final NewComment newCommentRecord = new NewComment(2L, "author", "content");

    public static final ObjectMapper mapper = jsonMapper();

    @Test
    void recordDeserializesProperly() throws Exception {
        final var mappedComment = mapper.readValue(newCommentJson, NewComment.class);
        assertThat(mappedComment).isEqualTo(newCommentRecord);
    }
}
