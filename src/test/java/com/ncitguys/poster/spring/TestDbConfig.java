package com.ncitguys.poster.spring;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.testcontainers.containers.PostgreSQLContainer;

@Configuration
@Profile("test")
@EnableJdbcRepositories("com.ncitguys.poster.db")
public class TestDbConfig {

    @Bean(destroyMethod = "stop")
    public PostgreSQLContainer<?> postgresContainer() {
        final var postgres = new PostgreSQLContainer<>("postgres:alpine");
        postgres.start();
        return postgres;
    }

    @Bean()
    public DataSource testDbDataSource(final PostgreSQLContainer<?> postgres,
            @Value("${test.db.init-scripts}") final FileSystemResource initScriptsDir)
            throws IOException, SQLException {
        final var testDbDataSourceProps = new DataSourceProperties();
        testDbDataSourceProps.setUrl(postgres.getJdbcUrl());
        testDbDataSourceProps.setUsername(postgres.getUsername());
        testDbDataSourceProps.setPassword(postgres.getPassword());
        final var db = testDbDataSourceProps.initializeDataSourceBuilder().build();
        final var conn = db.getConnection();

        for (final var file : initScriptsDir.getFile().listFiles()) {
            try (var fis = new FileInputStream(file)) {
                final var sql = new String(fis.readAllBytes());
                conn.createStatement().execute(sql);
            }
        }

        return db;
    }

}
