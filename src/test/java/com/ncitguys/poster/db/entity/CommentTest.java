package com.ncitguys.poster.db.entity;

import static com.ncitguys.poster.utils.TestUtils.jsonMapper;
import static com.ncitguys.poster.utils.TestUtils.prettyPrinter;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommentTest {
    private static final String COMMENT_JSON = """
            {
                "id" : 1,
                "commentOn" : 2,
                "replyTo" : null,
                "postedDate" : "2022-01-01T00:00:00Z",
                "lastEdited" : "2022-01-03T00:00:00Z",
                "author" : "author",
                "content" : "content",
                "published" : true,
                "replies" : [
                    {
                        "id" : 2,
                        "commentOn" : 2,
                        "replyTo" : 1,
                        "postedDate" : "2022-01-06T00:00:00Z",
                        "lastEdited" : "2022-01-08T00:00:00Z",
                        "author" : "author_2",
                        "content" : "content_2",
                        "published" : true,
                        "replies" : [ ]
                    }
                ]
            }""";

    private static final Comment COMMENT_RECORD = new Comment(
            1L,
            2L,
            null,
            OffsetDateTime.parse("2022-01-01T00:00:00Z"),
            OffsetDateTime.parse("2022-01-03T00:00:00Z"),
            "author",
            "content",
            true,
            List.of(new Comment(
                    2L,
                    2L,
                    1L,
                    OffsetDateTime.parse("2022-01-06T00:00:00Z"),
                    OffsetDateTime.parse("2022-01-08T00:00:00Z"),
                    "author_2",
                    "content_2",
                    true,
                    List.of())));

    private static final ObjectMapper mapper = jsonMapper();

    /**
     * Validate the comment serializes correctly
     */
    @Test
    void commentSerializesCorrectly() throws Exception {
        final var serializedComment = mapper
                .writer(prettyPrinter())
                .writeValueAsString(COMMENT_RECORD);
        assertThat(serializedComment).isEqualTo(COMMENT_JSON);
    }
}
