package com.ncitguys.poster.db.entity;

import static com.ncitguys.poster.utils.TestUtils.jsonMapper;
import static com.ncitguys.poster.utils.TestUtils.prettyPrinter;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PostTest {
    private static final String POST_JSON = """
            {
                "id" : 1,
                "postedDate" : "2022-01-01T00:00:00Z",
                "lastEdited" : "2022-01-03T00:00:00Z",
                "author" : "author",
                "content" : "content",
                "published" : true
            }""";

    private static final Post POST_RECORD = new Post(
            1L,
            OffsetDateTime.parse("2022-01-01T00:00:00Z"),
            OffsetDateTime.parse("2022-01-03T00:00:00Z"),
            "author",
            "content",
            true);

    private static final ObjectMapper mapper = jsonMapper();

    @Test
    void postSerializesCorrectly() throws Exception {
        final var serializedPost = mapper
                .writer(prettyPrinter())
                .writeValueAsString(POST_RECORD);
        assertThat(serializedPost).isEqualTo(POST_JSON);
    }

}
