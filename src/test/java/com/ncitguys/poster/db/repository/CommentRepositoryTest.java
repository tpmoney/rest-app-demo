package com.ncitguys.poster.db.repository;

import static com.ncitguys.poster.db.repository.CommentRepository.*;
import static com.ncitguys.poster.db.repository.CommentRepository.Columns.*;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.COMMENT_MAPPER;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.HEIRARCHICAL_COMMENT_EXTRATOR;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.mapToQueryParams;
import static java.time.ZoneOffset.UTC;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.ncitguys.poster.db.entity.Comment;

@ExtendWith(MockitoExtension.class)
public class CommentRepositoryTest {
    private static final Long POST_ID = 1L;
    private static final Long COMMENT_ID = 2L;
    private static final Long COMMENT_WITH_REPLY_ID = 3L;
    private static final String AUTHOR = "author";
    private static final String CONTENT = "content";
    private static final OffsetDateTime NOW = OffsetDateTime.now(UTC);
    private static final OffsetDateTime NOW_PLUS_5_MINUTES = NOW.plusMinutes(5);
    private static final OffsetDateTime NOW_PLUS_10_MINUTES = NOW.plusMinutes(10);
    private static final Comment COMMENT = new Comment(
            COMMENT_ID,
            POST_ID,
            COMMENT_WITH_REPLY_ID,
            NOW_PLUS_5_MINUTES,
            NOW_PLUS_10_MINUTES,
            "reply-" + AUTHOR,
            "reply-" + CONTENT,
            true,
            emptyList());
    private static final Comment COMMENT_WITH_REPLY = new Comment(
            COMMENT_WITH_REPLY_ID,
            POST_ID,
            null,
            NOW,
            NOW_PLUS_5_MINUTES,
            AUTHOR,
            CONTENT,
            true,
            List.of(COMMENT));

    @Mock
    private NamedParameterJdbcTemplate jdbcTemplate;

    @InjectMocks
    private CommentRepository commentRepository;

    @Captor
    private ArgumentCaptor<Map<String, Object>> paramCaptor;

    @Captor
    private ArgumentCaptor<String> queryCaptor;

    @Captor
    private ArgumentCaptor<RowMapper<Comment>> rowMapperCaptor;

    @Captor
    private ArgumentCaptor<ResultSetExtractor<Comment>> resultSetExtractorCaptor;

    /**
     * Validate that deleteById uses the correct query
     * and passes the correct parameters
     */
    @Test
    void testDeleteById() {
        commentRepository.deleteById(COMMENT_ID);

        verify(jdbcTemplate).update(queryCaptor.capture(), paramCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(DELETE_BY_ID_QUERY);
        assertThat(paramCaptor.getValue()).containsEntry(ID, COMMENT_ID);
    }

    /**
     * Validate that deleteByPost uses the correct query
     * and passes the correct parameters
     */
    @Test
    void testDeleteByPost() {
        commentRepository.deleteByPost(POST_ID);

        verify(jdbcTemplate).update(queryCaptor.capture(), paramCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(DELETE_BY_POST_QUERY);
        assertThat(paramCaptor.getValue()).containsEntry(COMMENT_ON, POST_ID);
    }

    /**
     * Validate that findByPostAndCommentId uses the correct query
     * and passes the correct parameters. Also validates that it
     * returns Empty if the query returns null.
     */
    @Test
    void testFindByPostAndCommentId() {

        final var result = commentRepository.findByPostAndCommentId(POST_ID, COMMENT_ID);

        assertThat(result).isEmpty();

        verify(jdbcTemplate).queryForObject(queryCaptor.capture(), paramCaptor.capture(), rowMapperCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(COMMENT_BY_POST_AND_ID_QUERY);
        assertThat(paramCaptor.getValue()).containsExactlyInAnyOrderEntriesOf(
                Map.of(COMMENT_ON, POST_ID,
                        ID, COMMENT_ID));
        assertThat(rowMapperCaptor.getValue()).isEqualTo(COMMENT_MAPPER);
    }

    /**
     * Validate that findByPostAndCommentId maps the results back to
     * the Comment object correctly.
     */
    @Test
    @SuppressWarnings("unchecked") // Can't do any(RowMapper<Comment>.class). This is safe
    void testFindByPostAndCommentIdMapsResults() {
        when(jdbcTemplate.queryForObject(eq(COMMENT_BY_POST_AND_ID_QUERY), anyMap(), any(RowMapper.class)))
                .thenAnswer(invocation -> {
                    final var mapper = invocation.getArgument(2, RowMapper.class);
                    return mapper.mapRow(mockResultSet(false), 1);
                });
        final var result = commentRepository.findByPostAndCommentId(POST_ID, COMMENT_ID);

        assertThat(result).contains(COMMENT);
    }

    /**
     * Validate that findByPostAndCommentId returns empty if
     * the query throws an EmptyResultDataAccessException
     */
    @Test
    @SuppressWarnings("unchecked") // Can't do any(RowMapper<Comment>.class). This is safe
    void testFindByPostAndCommentIdEmptyResult() {
        when(jdbcTemplate.queryForObject(eq(COMMENT_BY_POST_AND_ID_QUERY), anyMap(), any(RowMapper.class)))
                .thenThrow(EmptyResultDataAccessException.class);

        final var result = commentRepository.findByPostAndCommentId(POST_ID, COMMENT_ID);

        assertThat(result).isEmpty();
    }

    /**
     * Validate that findByPostAndCommentId does not intercept
     * other exceptions.
     */
    @Test
    @SuppressWarnings("unchecked") // Can't do any(RowMapper<Comment>.class). This is safe
    void testFindByPostAndCommentIdOtherException() {
        when(jdbcTemplate.queryForObject(eq(COMMENT_BY_POST_AND_ID_QUERY), anyMap(), any(RowMapper.class)))
                .thenThrow(new RuntimeException("big-bada-boom"));

        assertThatThrownBy(() -> commentRepository.findByPostAndCommentId(POST_ID, COMMENT_ID))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("big-bada-boom");
    }

    /**
     * Validate that findByPostId uses the correct query
     * and passes the correct parameters
     */
    @Test
    @SuppressWarnings("unchecked") // Can't do any(ResultSetExtractor<Comment>.class). This is safe
    void testFindByPostId() {
        when(jdbcTemplate.query(eq(ORDERED_COMMENT_QUERY), anyMap(), any(ResultSetExtractor.class)))
                .thenReturn(List.of(COMMENT_WITH_REPLY));

        final var result = commentRepository.findByPostId(POST_ID);

        assertThat(result).containsExactly(COMMENT_WITH_REPLY);

        verify(jdbcTemplate).query(queryCaptor.capture(), paramCaptor.capture(), resultSetExtractorCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(ORDERED_COMMENT_QUERY);
        assertThat(paramCaptor.getValue()).containsExactlyEntriesOf(
                Map.of(COMMENT_ON, POST_ID));
        assertThat(resultSetExtractorCaptor.getValue()).isEqualTo(HEIRARCHICAL_COMMENT_EXTRATOR);

    }

    /**
     * Validate that findByPostId maps the results back to
     * a proper heirarchical list. Specifically the list should
     * not contain any top level comments that are a reply to other
     * comments.
     */
    @Test
    @SuppressWarnings("unchecked") // Can't do any(ResultSetExtractor<Comment>.class). This is safe
    void testFindByPostIdMapsResults() {
        when(jdbcTemplate.query(eq(ORDERED_COMMENT_QUERY), anyMap(), any(ResultSetExtractor.class)))
                .thenAnswer(invocation -> {
                    final var extractor = invocation.getArgument(2, ResultSetExtractor.class);
                    return extractor.extractData(mockResultSet(true));
                });

        final var result = commentRepository.findByPostId(POST_ID);

        assertThat(result).contains(COMMENT_WITH_REPLY);
        assertThat(result).noneMatch(c -> c.replyTo() != null);
    }

    /**
     * Validate that the mapToQueryParams method returns
     * the right parameters
     */
    @Test
    void testMapToQueryParams() {
        final var result = mapToQueryParams(COMMENT);
        assertThat(result).containsExactlyInAnyOrderEntriesOf(
                Map.of(ID, COMMENT.id(),
                        COMMENT_ON, COMMENT.commentOn(),
                        REPLY_TO, COMMENT.replyTo(),
                        POSTED_DATE, COMMENT.postedDate(),
                        LAST_EDITED, COMMENT.lastEdited(),
                        AUTHOR, COMMENT.author(),
                        CONTENT, COMMENT.content(),
                        PUBLISHED, COMMENT.published()));
    }

    /**
     * Validate that save calls the insert query when
     * the new comment has a null ID
     */
    @Test
    void testSave() {
        final var commentWithoutId = COMMENT.toBuilder()
                .id(null)
                .build();
        commentRepository.save(commentWithoutId);

        verify(jdbcTemplate).queryForObject(queryCaptor.capture(), paramCaptor.capture(), rowMapperCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(INSERT_QUERY);
        assertThat(paramCaptor.getValue()).containsExactlyEntriesOf(
                mapToQueryParams(commentWithoutId));
        assertThat(rowMapperCaptor.getValue()).isEqualTo(COMMENT_MAPPER);
    }

    /**
     * Validate that save calls the update query when
     * the new comment has an ID
     */
    @Test
    void testSaveWithId() {
        commentRepository.save(COMMENT);

        verify(jdbcTemplate).queryForObject(queryCaptor.capture(), paramCaptor.capture(), rowMapperCaptor.capture());

        assertThat(queryCaptor.getValue()).isEqualTo(UPDATE_QUERY);
        assertThat(paramCaptor.getValue()).containsExactlyEntriesOf(
                mapToQueryParams(COMMENT));
        assertThat(rowMapperCaptor.getValue()).isEqualTo(COMMENT_MAPPER);
    }

    /**
     * Holds the collections of answers for use in testing
     */
    private record ResultSetAnswers(
            List<Long> ids,
            List<Long> postIds,
            List<Long> replyToIds,
            List<Timestamp> postedDates,
            List<Timestamp> lastEditedDates,
            List<String> authors,
            List<String> contents,
            List<Boolean> published) {
    };

    /**
     * Create a mock resultset for testing SQL mappers in the repository
     */
    private ResultSet mockResultSet(final boolean withReplies) {
        final var answers = new ResultSetAnswers(
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>());
        if (withReplies) {
            addAnswers(answers, COMMENT_WITH_REPLY, COMMENT);
        } else {
            addAnswers(answers, COMMENT);
        }
        try {
            final ResultSet result = mock(ResultSet.class);
            when(result.getLong(ID)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.ids));
            when(result.getLong(COMMENT_ON)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.postIds));
            when(result.getLong(REPLY_TO)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.replyToIds));
            when(result.getTimestamp(POSTED_DATE)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.postedDates));
            when(result.getTimestamp(LAST_EDITED))
                    .thenAnswer(AdditionalAnswers.returnsElementsOf(answers.lastEditedDates));
            when(result.getString(AUTHOR)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.authors));
            when(result.getString(CONTENT)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.contents));
            when(result.getBoolean(PUBLISHED)).thenAnswer(AdditionalAnswers.returnsElementsOf(answers.published));
            if (answers.replyToIds().contains(null) && answers.replyToIds().size() > 1) {
                final var wasNulls = new ArrayList<Boolean>();
                for (final Long replyToId : answers.replyToIds()) {
                    wasNulls.add(replyToId == null);
                }
                when(result.wasNull()).thenAnswer(AdditionalAnswers.returnsElementsOf(wasNulls));
            }
            if (answers.replyToIds().size() > 1) {
                final var nextCounts = new ArrayList<Boolean>();
                answers.replyToIds()
                        .stream()
                        .forEach(x -> nextCounts.add(true));
                nextCounts.add(false);
                when(result.next()).thenAnswer(AdditionalAnswers.returnsElementsOf(nextCounts));
            }
            return result;

        } catch (final Exception e) {
            throw new IllegalStateException("Unexpected test setup error!", e);
        }
    }

    /**
     * Adds the given comments to the given answer containers for use in
     * mocking the ResultSet.
     *
     * @param answers  the answer container record
     * @param comments the comments to add
     */
    private void addAnswers(final ResultSetAnswers answers, final Comment... comments) {
        for (final var comment : comments) {
            answers.ids.add(comment.id());
            answers.postIds.add(comment.commentOn());
            answers.replyToIds.add(comment.replyTo());
            answers.postedDates.add(Timestamp.from(comment.postedDate().toInstant()));
            answers.lastEditedDates.add(Timestamp.from(comment.lastEdited().toInstant()));
            answers.authors.add(comment.author());
            answers.contents.add(comment.content());
            answers.published.add(comment.published());
        }
    }
}
