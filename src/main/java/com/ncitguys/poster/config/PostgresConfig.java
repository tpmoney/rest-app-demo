package com.ncitguys.poster.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@Configuration(proxyBeanMethods = false)
@EnableJdbcRepositories("com.ncitguys.poster.db")
@Profile("!test")
public class PostgresConfig {

	/**
	 * The properties for the main database.
	 */
	// We need the @Primary annotation to help spring's auto configuration when
	// using multiple data named datasources
	@Primary
	@Bean
	@ConfigurationProperties("spring.datasource.maindb")
	public DataSourceProperties mainDbDataSourceProps() {
		return new DataSourceProperties();
	}

	/**
	 * The main database datasource
	 */
	// We need the @Primary annotation to help spring's auto configuration when
	// using multiple data named datasources
	@Primary
	@Bean
	public DataSource mainDbDataSource(
			@Qualifier("mainDbDataSourceProps") final DataSourceProperties mainDbDataSourceProps) {
		return mainDbDataSourceProps
				.initializeDataSourceBuilder()
				.build();
	}

}
