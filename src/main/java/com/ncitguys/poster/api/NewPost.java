package com.ncitguys.poster.api;

import lombok.Builder;

/**
 * A record that represents a JSON request for adding a new post into the system
 *
 * @param author  the name of the post author
 * @param content the content of the post
 */
@Builder(toBuilder = true)
public record NewPost(String author, String content) {

}
