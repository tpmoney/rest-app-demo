package com.ncitguys.poster.api;

import lombok.Builder;

/**
 * A record that represents a JSON request for adding a new comment into the
 * system
 *
 * @param replyTo the ID of the comment this comment is a reply to, if any
 * @param author  the name of the comment author
 * @param content the content of the comment
 */
@Builder(toBuilder = true)
public record NewComment(
        Long replyTo,
        String author,
        String content) {
}
