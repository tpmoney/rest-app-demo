package com.ncitguys.poster.db.entity;

import java.time.OffsetDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Builder;

/**
 * A record representing a post
 *
 * @param id         the ID of the post
 * @param postedDate the date that this post was posted
 * @param lastEdited the date that this post was last edited
 * @param author     the name of the author of this post
 * @param content    the content of the post
 * @param published  whether or not this post is published and viewable
 */
@Table("posts")
@Builder(toBuilder = true)
public record Post(
		@Id Long id,
		@Column("posted_date") OffsetDateTime postedDate,
		@Column("last_edited") OffsetDateTime lastEdited,
		String author,
		String content,
		Boolean published) {
};
