package com.ncitguys.poster.db.entity;

import java.time.OffsetDateTime;
import java.util.List;

import lombok.Builder;

/**
 * A record representing a comment on a post.
 * <p/>
 * Note that the `replies` field is a read only representation of the
 * replies to this comment. It will not be persisted for saves to the database
 * and requires special handling when querying the DB to fill in.
 *
 * @param id         the ID of the comment
 * @param commentOn  the ID of the post that this comment is attached to
 * @param replyTo    the ID of the comment that this comment is a reply to if
 *                   any
 * @param postedDate the date that this comment was posted
 * @param lastEdited the date that this comment was last edited
 * @param author     the name of the author of this comment
 * @param content    the content of the comment
 * @param published  whether or not this comment is published and viewable
 * @param replies    the replies to this comment (read only)
 */
@Builder(toBuilder = true)
public record Comment(
		Long id,
		Long commentOn,
		Long replyTo,
		OffsetDateTime postedDate,
		OffsetDateTime lastEdited,
		String author,
		String content,
		Boolean published,
		List<Comment> replies) {
}
