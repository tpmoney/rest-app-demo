package com.ncitguys.poster.db.repository;

import static com.ncitguys.poster.db.repository.CommentRepository.Columns.*;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.COMMENT_MAPPER;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.HEIRARCHICAL_COMMENT_EXTRATOR;
import static com.ncitguys.poster.db.repository.CommentRepository.Mappers.mapToQueryParams;
import static java.time.ZoneOffset.UTC;

import java.time.OffsetDateTime;
import java.util.*;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.ncitguys.poster.db.entity.Comment;

/**
 * The repository for querying the `comments` table in the database
 */
@Component
public class CommentRepository {
    protected static final String ORDERED_COMMENT_QUERY = """
            SELECT *
            FROM comments
            WHERE comment_on = :comment_on
            AND published = true
            AND EXISTS (
                SELECT 1
                FROM posts
                WHERE id = :comment_on
                AND published = true)
            ORDER BY reply_to NULLS FIRST, posted_date DESC
            """;

    protected static final String COMMENT_BY_POST_AND_ID_QUERY = """
            SELECT *
            FROM comments
            WHERE id = :id
            AND comment_on = :comment_on
            AND published = true
            AND EXISTS (
                SELECT 1
                FROM posts
                WHERE id = :comment_on
                AND published = true)
            """;

    protected static final String INSERT_QUERY = """
            INSERT INTO comments (comment_on, reply_to, author, content, posted_date, last_edited, published)
            VALUES (:comment_on, :reply_to, :author, :content, :posted_date, :last_edited, :published)
            RETURNING *
            """;

    protected static final String UPDATE_QUERY = """
            UPDATE comments
            SET comment_on = :comment_on,
                reply_to = :reply_to,
                posted_date = :posted_date,
                last_edited = :last_edited,
                author = :author,
                content = :content,
                published = :published
            WHERE id = :id
            RETURNING *
            """;

    protected static final String DELETE_BY_POST_QUERY = """
            DELETE FROM comments
            WHERE comment_on = :comment_on
            """;

    protected static final String DELETE_BY_ID_QUERY = """
            DELETE FROM comments
            WHERE id = :id
            """;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public CommentRepository(final NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Query for getting all the comments for a given post
     * <p/>
     * By ordering the comments in order of their `reply_to` field and sorting
     * the null values first, we are able to guarantee when using the
     * {@link HIERARCHICAL_COMMENT_EXTRATOR} that parent replies already
     * exist in the map so that we can add their children to them.
     *
     * @param postId the ID of the post to get comments for
     * @return a list of comments on the given post
     */
    public List<Comment> findByPostId(final Long postId) {
        final var params = Map.of(COMMENT_ON, postId);
        return jdbcTemplate.query(ORDERED_COMMENT_QUERY, params, HEIRARCHICAL_COMMENT_EXTRATOR);
    }

    /**
     * Query for getting a single comment by post and comment ID
     *
     * @param postId    the ID of the post the comment is on
     * @param commentId the ID of the comment to get
     * @return the comment with the given ID, or empty if there is no comment
     *         with that ID in the database
     */
    public Optional<Comment> findByPostAndCommentId(final Long postId, final Long commentId) {
        final var params = Map.of(
                COMMENT_ON, postId,
                ID, commentId);
        try {
            return Optional.ofNullable(
                    jdbcTemplate.queryForObject(
                            COMMENT_BY_POST_AND_ID_QUERY, params, COMMENT_MAPPER));
        } catch (final EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    /**
     * Save a new comment to the database
     *
     * @param newComment the new comment to be saved
     * @return the new comment with its new ID
     */
    public Comment save(final Comment newComment) {
        if (newComment.id() == null) {
            return jdbcTemplate.queryForObject(INSERT_QUERY, mapToQueryParams(newComment), COMMENT_MAPPER);
        } else {
            return jdbcTemplate.queryForObject(UPDATE_QUERY, mapToQueryParams(newComment), COMMENT_MAPPER);
        }
    }

    /**
     * Delete all comments for a given post
     *
     * @param postId the post to delete comments for
     */
    public int deleteByPost(final Long postId) {
        return jdbcTemplate.update(DELETE_BY_POST_QUERY, Map.of(COMMENT_ON, postId));
    }

    /**
     * Delete a comment by ID
     *
     * @param id the ID of the comment to delete
     */
    public int deleteById(final Long id) {
        return jdbcTemplate.update(DELETE_BY_ID_QUERY, Map.of(ID, id));
    }

    protected static class Mappers {
        private Mappers() {
            // Utility class, no instantiation
        }

        /**
         * A simple mapping of comment data in the database to a single comment
         * record. This sets the `replies` field to a new empty list as we don't
         * have any information in a single row to fill that in.
         */
        static RowMapper<Comment> COMMENT_MAPPER = (rs, rowNum) -> {
            // getLong automatically converts null to 0, but we
            // want to ensure we're getting nulls back when they're
            // actually null in the database
            Long replyTo = rs.getLong(REPLY_TO);
            if (rs.wasNull()) {
                replyTo = null;
            }
            return new Comment(
                    rs.getLong(ID),
                    rs.getLong(COMMENT_ON),
                    replyTo,
                    OffsetDateTime.ofInstant(rs.getTimestamp(POSTED_DATE).toInstant(), UTC),
                    OffsetDateTime.ofInstant(rs.getTimestamp(LAST_EDITED).toInstant(), UTC),
                    rs.getString(AUTHOR),
                    rs.getString(CONTENT),
                    rs.getBoolean(PUBLISHED),
                    new ArrayList<>());

        };

        /**
         * Extracts a hierachical list of comments from a ResultSet. This
         * assumes that the results have been sorted in order of `reply_to` with
         * the nulls sorted first. This ensures that parent comments exist in the
         * intermediate mapping before their children so we can ensure they're
         * added to the hierarchy correctly.
         */
        static ResultSetExtractor<List<Comment>> HEIRARCHICAL_COMMENT_EXTRATOR = rs -> {
            final var comments = new HashMap<Long, Comment>();

            while (rs.next()) {
                final var comment = COMMENT_MAPPER.mapRow(rs, rs.getRow());
                comments.put(comment.id(), comment);
                if (comment.replyTo() != null) {
                    comments.get(comment.replyTo()).replies().add(comment);
                }
            }
            // Return a list of only top level comments. The replies are linked
            // inside the `replies` field
            return comments.values()
                    .stream()
                    .filter(c -> c.replyTo() == null)
                    .toList();
        };

        static Map<String, Object> mapToQueryParams(final Comment comment) {
            final var params = new HashMap<String, Object>();
            params.put(ID, comment.id());
            params.put(COMMENT_ON, comment.commentOn());
            params.put(REPLY_TO, comment.replyTo());
            params.put(POSTED_DATE, comment.postedDate());
            params.put(LAST_EDITED, comment.lastEdited());
            params.put(AUTHOR, comment.author());
            params.put(CONTENT, comment.content());
            params.put(PUBLISHED, comment.published());
            return params;
        }
    }

    /**
     * String constants for column names. We can't use them everywhere until
     * we until java string templates are finalized but they'll help keep
     * column names in sync. If you change any of these, make sure you
     * also change the SQL queries
     */
    protected static class Columns {
        private Columns() {
            // Utility class, no instantiation
        }

        static final String ID = "id";
        static final String COMMENT_ON = "comment_on";
        static final String REPLY_TO = "reply_to";
        static final String POSTED_DATE = "posted_date";
        static final String LAST_EDITED = "last_edited";
        static final String AUTHOR = "author";
        static final String CONTENT = "content";
        static final String PUBLISHED = "published";
    }

}
