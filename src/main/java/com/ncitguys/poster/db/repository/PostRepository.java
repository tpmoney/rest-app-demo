package com.ncitguys.poster.db.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.RepositoryDefinition;

import com.ncitguys.poster.db.entity.Post;

/**
 * The repository for accessing post data in the database
 */
@RepositoryDefinition(domainClass = Post.class, idClass = Long.class)
public interface PostRepository {

	/**
	 * Find a post by its ID
	 *
	 * @param id the ID of the post
	 * @return the post with the given ID, or empty if there is no post with
	 *         with that ID in the database
	 */
	Optional<Post> findByIdAndPublishedIsTrue(Long id);

	/**
	 * Find all posts by a given author
	 *
	 * @param author the name of the author to search by
	 * @return a list of posts by the given author, or an empty list if there
	 *         are no posts by the given author
	 */
	List<Post> findByAuthorAndPublishedIsTrueOrderByPostedDateDesc(String author);

	/**
	 * Save a new post to the database
	 *
	 * @param newPost the post to save
	 * @return the saved post with its new ID
	 */
	Post save(Post newPost);

	/**
	 * Query to get all the posts ordered by their `posted_date` field
	 */
	@Query("SELECT * FROM posts ORDER BY posted_date DESC")
	List<Post> getAllOrderByPostedDateDesc();

	void deleteById(Long id);
}
