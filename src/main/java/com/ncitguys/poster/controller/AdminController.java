package com.ncitguys.poster.controller;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.db.repository.CommentRepository;
import com.ncitguys.poster.db.repository.PostRepository;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final PostRepository postRepo;
    private final CommentRepository commentRepo;

    public AdminController(final PostRepository postRepo, final CommentRepository commentRepo) {
        this.postRepo = postRepo;
        this.commentRepo = commentRepo;
    }

    /**
     * A REST endpoint to view all posts, regardless of whether they are published
     * or not
     * 
     * @param id
     */
    @GetMapping("/posts")
    public List<Post> getAllPosts() {
        return postRepo.getAllOrderByPostedDateDesc();
    }

    /**
     * A REST endpoint to delete a post permanently from the database.
     * <p/>
     * Because this eliminates the entire post, it also removes all the comments
     * that belong to it.
     *
     * @param id the ID of the post to delete
     */
    @DeleteMapping("/posts/{id}")
    @Transactional
    public void deletePost(@PathVariable("id") final Long id) {
        commentRepo.deleteByPost(id);
        postRepo.deleteById(id);
    }

    /**
     * A REST endpoint to delete a comment permanently from the database.
     * <p/>
     * Because comments that lose their parent get their `reply_to` field set
     * to null, they become top level comments on the post and so this endpoint
     * does not remove them.
     *
     * @param id the ID of the comment to delete
     */
    @DeleteMapping("/comments/{id}")
    public void deleteComment(@PathVariable("id") final Long id) {
        commentRepo.deleteById(id);
    }

}
