package com.ncitguys.poster.controller;

import static com.ncitguys.poster.util.PostUtils.toComment;
import static com.ncitguys.poster.util.PostUtils.toPost;
import static java.time.ZoneOffset.UTC;
import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.time.Clock;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ncitguys.poster.api.NewComment;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.db.entity.Comment;
import com.ncitguys.poster.db.entity.Post;
import com.ncitguys.poster.db.repository.CommentRepository;
import com.ncitguys.poster.db.repository.PostRepository;

/**
 * The main REST controller for the application.
 * <p/>
 * All the requests for the application route through the `/post` path as
 * our chosen REST implementation.
 */
@RestController
@RequestMapping(path = "/posts")
public class PostController {

	private final PostRepository postRepo;
	private final CommentRepository commentRepo;
	private final Clock clock;

	/**
	 * Create a new PostController
	 * <p/>
	 * This is autowired by spring.
	 *
	 * @param postRepo    the PostRepository the repository for accessing post data
	 *                    in the database
	 * @param commentRepo the CommentRepository the repository for accessing comment
	 *                    data in the database
	 */
	public PostController(final PostRepository postRepo, final CommentRepository commentRepo, final Clock clock) {
		this.postRepo = postRepo;
		this.commentRepo = commentRepo;
		this.clock = clock;
	}

	/**
	 * REST endpoint for getting a single post by ID
	 *
	 * @param id the ID of the post to get
	 * @return a {@link Post} representing the post, or a 404 response if there
	 *         is no post with the given ID
	 */
	@GetMapping("/{id}")
	@ResponseBody
	public Post getPost(@PathVariable("id") final Long id) {
		return postRepo.findByIdAndPublishedIsTrue(id)
				.orElseThrow(() -> new ResponseStatusException(
						NOT_FOUND, "Post with ID %s not found".formatted(id)));
	}

	/**
	 * REST endpoint for getting all posts by a given author
	 *
	 * @param name the name of the author to search for
	 * @return a list of all {@link Post}s written by the given author or an
	 *         empty list if there are no such posts
	 */
	@GetMapping("/author/{name}")
	@ResponseBody
	public List<Post> getByAuthor(@PathVariable("name") final String name) {
		return postRepo.findByAuthorAndPublishedIsTrueOrderByPostedDateDesc(name);
	}

	/**
	 * REST endpoint for getting all comments on for a given post
	 * <p/>
	 * Comments are returned as a hierarchical list of comments with replies to
	 * each comment as nested children.
	 *
	 * @param id the ID of the post to get comments for
	 * @return a list of all comments for the given post, or an empty list if
	 *         there are no comments on the post
	 */
	@GetMapping("/{id}/comments")
	@ResponseBody
	public List<Comment> getComments(@PathVariable("id") final Long id) {
		return commentRepo.findByPostId(id);
	}

	/**
	 * REST endpoint for uploading a new post
	 * <p/>
	 * Per the rest stanard, we include a URI to the newly created post in the
	 * Location header of the response. This URI is constructed from the current
	 * request's URI and so should be valid for the user. If this controller is
	 * placed behind an internal proxy, we would need to validate that the
	 * returned URI matches what the user requested, not what the proxy
	 * requested.
	 *
	 * @param newPostRequest the content of the new post
	 * @return the newly created {@link Post}
	 */
	@PostMapping
	public ResponseEntity<Post> uploadPost(@RequestBody final NewPost newPostRequest) {

		if (isBlank(newPostRequest.content())) {
			throw new ResponseStatusException(BAD_REQUEST, "Post content cannot be empty");
		}

		if (isBlank(newPostRequest.author())) {
			throw new ResponseStatusException(BAD_REQUEST, "Post author cannot be empty");
		}

		final var newPost = postRepo.save(toPost(newPostRequest, clock));
		final var newPostUri = ServletUriComponentsBuilder.newInstance()
				.path("./{id}")
				.build(newPost.id());

		return ResponseEntity.created(newPostUri)
				.body(newPost);

	}

	/**
	 * REST endpoint for updating an existing post
	 *
	 * @param postId         the ID of the post to update
	 * @param newPostRequest the new content of the post
	 * @return the updated {@link Post}
	 */
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> updatePost(
			@PathVariable("id") final Long postId,
			@RequestBody final NewPost newPostRequest) {

		if (isBlank(newPostRequest.content())) {
			throw new ResponseStatusException(BAD_REQUEST, "Post content cannot be empty");
		}

		final var existingPost = postRepo.findByIdAndPublishedIsTrue(postId)
				.orElseThrow(() -> new ResponseStatusException(
						NOT_FOUND, "Post with ID %s not found".formatted(postId)));

		if (!existingPost.author().equals(newPostRequest.author())) {
			throw new ResponseStatusException(
					BAD_REQUEST, "User %s is not the author of post %s".formatted(newPostRequest.author(), postId));
		}

		final var updatedPost = existingPost.toBuilder()
				.content(newPostRequest.content())
				.lastEdited(clock.instant().atOffset(UTC))
				.build();
		postRepo.save(updatedPost);

		return ResponseEntity.noContent().build();
	}

	/**
	 * REST endpoint for un-publishing a post
	 * <p/>
	 * Since this is a user endpoint, we soft delete the post rather than
	 * actually deleting it from the DB.
	 *
	 * @param postId the ID of the post to delete
	 */
	@DeleteMapping("/{id}")
	@Transactional
	public void unpublishPost(@PathVariable("id") final Long postId) {
		final var post = postRepo.findByIdAndPublishedIsTrue(postId)
				.orElseThrow(() -> new ResponseStatusException(
						NOT_FOUND, "Post with ID %s not found".formatted(postId)));
		final var updatedPost = post.toBuilder().published(false).build();
		postRepo.save(updatedPost);
	}

	/**
	 * REST endpoint for adding a new comment to a post
	 * <p/>
	 * As in {@link #uploadPost}, we include a URI to the newly created comment
	 * with the same caveats
	 * <p/>
	 * The database requires that a reply to comment must be a reply to a
	 * comment on the same post. If the given `replyTo` value in the request is
	 * not null and the comment being replied to is not a comment on this post
	 * the save will be rejected with an exception.
	 *
	 * @param newCommentRequest the content of the new comment
	 * @return the newly created {@link Comment}
	 * @see {@link ExceptionHandlers#handleDbException(Exception)}
	 */
	@PostMapping("/{id}/comments")
	public ResponseEntity<Comment> uploadComment(
			@PathVariable("id") final Long postId,
			@RequestBody final NewComment newCommentRequest) {

		if (isBlank(newCommentRequest.content())) {
			throw new ResponseStatusException(BAD_REQUEST, "Comment content cannot be empty");
		}

		if (isBlank(newCommentRequest.author())) {
			throw new ResponseStatusException(BAD_REQUEST, "Comment author cannot be empty");
		}

		// The database will handle validating that the reply is to a comment
		// on the same post. If not it will throw a DataIntegrityViolationException
		// which will be handled by the global exception handler

		final var newComment = commentRepo.save(toComment(postId, newCommentRequest, clock));
		final var newCommentUri = ServletUriComponentsBuilder.newInstance()
				.path("./{commentId}")
				.build(newComment.id());

		return ResponseEntity.created(newCommentUri)
				.body(newComment);
	}

	/**
	 * REST endpoint for updating an existing comment
	 * <p/>
	 * Of note, this does not let the user change which comment this comment is
	 * a reply to, just the content of the comment text
	 *
	 * @param postId     The ID of the post this comment is on
	 * @param commentId  The ID of the comment
	 * @param newComment The new content of the comment
	 * @return
	 */
	@PutMapping("/{id}/comments/{commentId}")
	@Transactional
	public ResponseEntity<Void> updateComment(
			@PathVariable("id") final Long postId,
			@PathVariable("commentId") final Long commentId,
			@RequestBody final NewComment newComment) {

		if (isBlank(newComment.content())) {
			throw new ResponseStatusException(BAD_REQUEST, "Comment content cannot be empty");
		}

		final var existingComment = commentRepo.findByPostAndCommentId(postId, commentId)
				.orElseThrow(() -> new ResponseStatusException(
						NOT_FOUND, "Comment with ID %s for post %s not found".formatted(commentId, postId)));

		if (!existingComment.author().equals(newComment.author())) {
			throw new ResponseStatusException(
					BAD_REQUEST, "User %s is not the author of comment %s".formatted(newComment.author(), commentId));
		}

		final var updatedComment = existingComment.toBuilder()
				.content(newComment.content())
				.lastEdited(clock.instant().atOffset(UTC))
				.build();
		commentRepo.save(updatedComment);

		return ResponseEntity.noContent().build();
	}

	/**
	 * A REST endpoint for un-publishing a comment
	 * <p/>
	 * Since this is a user endpoint, we soft delete the comment rather than
	 * actually deleting it from the DB.
	 *
	 * @param postId    The ID of the post this comment is on
	 * @param commentId The ID of the comment
	 */
	@DeleteMapping("/{id}/comments/{commentId}")
	@Transactional
	public void unpublishComment(
			@PathVariable("id") final Long postId,
			@PathVariable("commentId") final Long commentId) {

		final var comment = commentRepo.findByPostAndCommentId(postId, commentId)
				.orElseThrow(() -> new ResponseStatusException(
						NOT_FOUND, "Comment with ID %s for post %s not found".formatted(commentId, postId)));

		final var updatedComment = comment.toBuilder().published(false).build();
		commentRepo.save(updatedComment);
	}
}
