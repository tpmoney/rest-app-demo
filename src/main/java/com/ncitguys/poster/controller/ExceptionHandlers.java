package com.ncitguys.poster.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.UUID;

import org.postgresql.util.PSQLException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ExceptionHandlers {
    @ExceptionHandler({ DataIntegrityViolationException.class, PSQLException.class })
    public ResponseEntity<String> handleDbException(final Exception e) {
        final var errorId = UUID.randomUUID();
        log.error("Database error id: {}", errorId, e);
        if (e instanceof final DataIntegrityViolationException dive) {
            if (dive.getMessage().contains("violates foreign key constraint \"comments_comment_on_reply_to_fkey\"")) {
                return ResponseEntity.status(BAD_REQUEST)
                        .body("replyTo value is not a comment on this post. Error ID: " + errorId);
            }

            if (dive.getMessage().contains("violates foreign key constraint \"comments_comment_on_fkey\"")) {
                return ResponseEntity.status(NOT_FOUND)
                        .body("Cannot add comment to non-existent post. Error ID: " + errorId);
            }
        }
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Database error. Error ID: " + errorId);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<String> handleResponseStatusException(final ResponseStatusException e) {
        final var errorId = UUID.randomUUID();
        log.error("Request Error id: {}", errorId, e);
        return ResponseEntity.status(e.getStatusCode()).body("Error ID: %s\n%s".formatted(errorId, e.getReason()));
    }

    @ExceptionHandler
    public ResponseEntity<String> handleAllOtherExceptions(final Exception e) {
        final var errorId = UUID.randomUUID();
        log.error("Error id: {}", errorId, e);
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body("Unexpected Error. Error ID: " + errorId);
    }
}
