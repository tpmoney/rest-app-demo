package com.ncitguys.poster.util;

import static java.time.ZoneOffset.UTC;

import java.time.Clock;

import com.ncitguys.poster.api.NewComment;
import com.ncitguys.poster.api.NewPost;
import com.ncitguys.poster.db.entity.Comment;
import com.ncitguys.poster.db.entity.Post;

/**
 * Utilities for manipulating {@link Post} data
 */
public class PostUtils {
	private PostUtils() {
		// Utility class, no instantiation
	}

	/**
	 * Convert a {@link NewPost} to a {@link Post} for saving into the database
	 *
	 * @param newPost the JSON request from the API
	 * @return the final {@link Post} that can be saved to the DB
	 */
	public static Post toPost(final NewPost newPost, final Clock clock) {
		final var now = clock.instant().atOffset(UTC);
		return new Post(
				null,
				now,
				now,
				newPost.author(),
				newPost.content(),
				true);
	}

	/**
	 * Convert a {@link NewComment} to a {@link Comment} for saving into the
	 * database
	 *
	 * @param postId     the post this comment is for
	 * @param newComment the JSON request from the API
	 * @return the final {@link Comment} that can be saved to the DB
	 */
	public static Comment toComment(final Long postId, final NewComment newComment, final Clock clock) {
		final var now = clock.instant().atOffset(UTC);
		return new Comment(null,
				postId,
				newComment.replyTo(),
				now,
				now,
				newComment.author(),
				newComment.content(),
				true,
				null);
	}
}
