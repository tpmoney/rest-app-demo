# Poster Project

A basic post and comment CRUD demo for Java / Spring

## Prerequisites

To build and run this project, you will need gradle and docker. If gradle isn't
installed on your system, the `gradlew` or `gradlew.bat` scripts in the project
root directory should run the bundled distribution for you on linux and windows
respectively. The rest of this document assumes running on linux or macos.
Docker is required for running the application database locally and for running
the integration tests. If you wish to build the project without running the
tests, you will need to pass the `-x test` argument to the `./gradlew` command.

## Compiling and Running Tests

From the project root, run `./gradlew clean build test` to execute a new build
of the project and run all of the unit and integration tests. Coverage reports
and test reposts will be generated in `build/reports`. As indicated in the
`build.gradle` file, the tests provide 100% line and branch coverage of all
files except classes in `src/test/` and any Spring configuration files in
`src/main`.

## Running the Application Locally

There are 3 main ways to run the application locally from easy to hard.

### Easiest

From the root project directory run `./gradlew clean bootJar` to build the
jar file that the docker image will need to run the application.

In the `utils` directory, run `docker-compose up -d`. This will spin up docker
containers for both the main application and the postgres database. It will load
a small amount of test data from `db/test_data` as well. The application is
exposed on port 8080 and the database on port 5432. When you are done, you can
run `docker-compose down --volumes` to tear down the containers and remove the
volumes created for the database.

### Less Easy

In the `utils` directory, run `docker-compose up flyway-test-data -d`. This will
spin up only the postgres database and populate it with the test data. From here
you can start the application either with gradle by running `./gradlew bootRun`
or by loading the project up in your IDE of choice and running the application
from there. In either case, the application defaults to looking for the database
at `localhost:5432`. See the `application.yaml` file in `src/main/resources`.

As above, `docker-compose down --volumes` when you're done will tear down the
database and remove the volume.

Additionally if you want to run the database but with only a blank schema, you
can instead run `docker-compose up flyway-schema -d`

### The Hard Way

If you want to run without docker, you will need to have a running postgres
instance available. You should at a minimum run the sql files in
`db/migrations` against that instance and if you want the test data then also
the sql files in `db/test_data`. Flyway would be a simple way to do this, but
the files are plain SQL so they could be run with `psql` or any other client.

Once the database is running, you'll want to make sure the `application.yaml`
file has the URL and credentials for the database. You may want to create a new
`application-local.yaml` file to provide overrides for the default values. If
you choose to do that, make sure you run the application with the argument
`--spring.profiles.active=local` in order to make sure that configuration is
loaded.

From this point, you will as above want to run the application.
`./gradlew bootRun` is the easiest way to do that, but you can also run from
your IDE, or if you run `./gradlew bootJar`, you can manually execute the
generated jar file with `java -jar build/libs/poster-app.jar`.

## The Endpoints

There are two main sets of endpoints for this project, the user endpoints and
the administrative endpoints. They are described in more detail below. The
simple way to access these endpoints is with the `curl` command. For GET
requests, the command will look like:
`curl localhost:8080/path/to/endpoint`

For POST/PUT requests, the application accepts JSON bodies. To send such a
request the command will look like:
`curl -X ${method} localhost:8080/path/to/endpoint --json ${body}`
where ${method} is either `POST` or `PUT` and ${body} is a JSON string.

Lastly DELETE endpoint requests will be made with a command like:
`curl -X DELETE localhost:8080/path/to/endpoint`

For all curl commands you can pass the `-v` argument to see HTTP status and
header information from the requests.

For admin endpoints where credentials are needed you can add the argument
`--user "username:password"` replacing the username and password as appropriate.

### User Endpoints

None of these endpoints require authentication

#### Post Management

- `GET /posts/{id}`
  This will retrieve a single post from the application by its ID. It will be
  returned as a JSON object with the following structure:

  ```json
  {
    "id": "${the id of the post}",
    "postedDate" : "${the date / time the post was submitted to the app}",
    "lastEdited" : "${the date / time the post was last edited}",
    "author" : "${the name of the author of the post}",
    "content" : "${the content of the post}",
    "published": "${whether or not the post is published}",
  }
  ```

  Note that all posts retrieved from this endpoint must be published posts.
  The application will not return unpublished posts at this endpoint.

- `GET /posts/author/{name}`
  This will retrieve all posts from the application by the author's name. If
  will return a list of posts as described above in descending posted_date
  order. The structure of the individual posts will be as described above and
  this endpoint is also limited only to published posts.

- `POST /posts`
  This will submit a new post to the application to be published. The post
  request should send a JSON body with the following structure:

  ```json
  {
    "author": "${the name of the author of the post}",
    "content": "${the content of the post}",
  }
  ```

  If successfully published, the response will contain the newly publised
  post in the same format as described above, and it will contain a location
  header with a relative path to the new post.

- `PUT /posts/{id}`
  This endpoitn will allow updating the content of an existing post. The
  request should send a JSON body with the same structure as the above `POST`
  endpoint. This endpoint will not allow updating a post with empty content
  nor will it allow updates by an author that is not the original post author.
  Attempting to do either will result in a `400 BAD REQUEST` response. Trying
  to update a post that does not exist will result in a `404 NOT FOUND`
  response.

- `DELETE /posts/{id}`
  This endpoint will allow a user to make a post "un-published". The post will
  not be deleted from the database, but will no longer be accessible from the
  `GET` endpoints. Attempting to delete a post that does not exist will result
  in a `404 NOT FOUND` response.

#### Comment Management

- `GET /posts/{id}/comments`
  This endoint will retrieve all comments for a given post. The response will
  be a list of comment objects with the following structure:

  ```json
  {
    "id": "${the id of the comment}",
    "comment_on" : "${the id of the post the comment is attached to}",
    "reply_to" : "${the id of the comment this is a reply to, if any}",
    "postedDate": "${the date / time the comment was posted}",
    "lastEdited": "${the date / time the comment was last edited}",
    "author": "${the author of the comment}",
    "content": "${the content of the comment}",
    "published": "${whether or not the comment is published}",
    "replies": "${a list of replies to the comment, if any}",
  }
  ```

  All top level comments in the list will be comments on the post that are not
  replies to other comments. Replies to comments will be inside each parent
  comment's `replies` list. As with the post management endpoints, this
  endpoint is limited to published comments. Comments will be ordered by
  `postedDate` in descending order.

- `POST /posts/{id}/comments`
  This endpoint will allow a user to make a new comment on a post. The request
  should send a JSON body with the following structure:

  ```json
  {
    "replyTo" : "${id of the comment this is a reply to, if any, or null}",
    "author": "${the name of the author of the comment}",
    "content": "${the content of the comment}",
  }
  ```

  Like the other `POST` enpoints, this will not allow blank comments or
  authors. Attempting to add a comment to a non-existing post will result in
  a `404 NOT FOUND` response. Attempting to add a comment as a reply to a
  comment that is attached to a different post than the one identified in the
  request path will result in a `400 BAD REQUEST` response.

- `PUT /posts/{id}/comments/{id}`
  This endpoint will allow updating the content of an existing comment. The
  request should send a JSON body with the same structure as the above
  `POST` endpoint. This endpoint will not allow updating a comment with empty
  content nor will it allow updates by an author that is not the original
  comment author. Attempting to do either will result in a `400 BAD REQUEST`
  response. Trying to update a comment that does not exist will result in a
  `404 NOT FOUND` response. This endpoint also does not change the `replyTo`
  field of a comment. Any supplied `replyTo` value will be ignored.

- `DELETE /posts/{id}/comments/{id}`
  This endpoint will allow a user to "un-publish" a comment. The comment will
  not be deleted from the database, but will no longer be accessible from the
  `GET` endpoints. Attempting to delete a comment that does not exist will result in a `404 NOT FOUND` response.

### Admin Endpoints

Unlike the user endpoints, the admin endpoint require authentication. The
default credentials for the demo app are `admin` for the username and
`super-secret` for the password. This can be changed in the
`WebSecurityConfig` class and should definitely be changed before this app
is be released to any production environments.

- `GET /admin/posts`
  This endpoint will retrieve a list of all posts in the system, regardless
  of their `published` status. The post structure will be as described in the
  User Endpoints section above.

- `DELETE /admin/posts/{id}`
  This endpoint will delete a post from the database. Because the post will be
  removed from the DB permanently, this endpoint will also delete any
  associated comments in the database and all of their replies as well. As
  discussed in the [Errors on Delete] section below, this endpoint will return
  a `200 OK` for requests to delete non-existent posts.

- `DELETE /admin/comments/{id}`
  This endpoint will delete a comment from the database. If there are any
  comments that are replies to this comment, they will have their `replyTo`
  fields nulled out and they will become "top level" comments on their
  associated post. Like the `DELETE /admin/posts/{id}` endpoint, this endpoint
  this endpoint will also return a `200 OK` for requests to delete comments
  that don't exist.

## Notable Design Decisions, Limitations and Future Concerns

### Project Structure

The Project is organized into the following packages

```text
* com.ncitguys.poster
|- api
|- config
|- controller
|-db
|  |- entity
|  |- repository
|- util
```

The `api` package contains the records that represent the JSON object we expect
to take in on our API. This package could optionally have been placed in a
package with, or as a sub-package of the `controller` package as they are
related. It also could have included the items found in the `db.entity` package
since we use those objects as API return values as well. In a larger real world
application, the return values of the API would probably not be the same as the
DB DTO objects.

The `config` package contains the various `@Configuration` annotated classes
that provide the beans and other autowired object we want Spring to be able to
inject for us.

The `controller` package contains the `@RestController` annotated classes and
associated code to handle incoming requests to the application. This represents
the front end of our application for the users. While the various controllers
are calling out to the repositories directly, a larger real world application
would probably have some additional business logic between the database layer
and the API layer and these controllers would more likely be calling out to
"Services" in another package, and those service classes would coordinate with
the databases for the information they need. The controller classes would then
largely just be responsible for validating request input and converting the
objects returned from the services into the customer facing API objects.

The `db` package contains the classes that interact with our database. The
`entity` sub-package contains the DTO objects that are used to represent database
records in the application. The `repository` sub-package contains the classes
that actually query the database. Whether those are Sprint Data JDBC interfaces
or real classes that make queries using a `JdbcTemplate` or `Connection` to the
database. These classes turn the DTOs into records for saving to the database
and likewise turn database rows into records for the application.

The `util` package contains the various utility classes that are used to provide
some common static functionality to the other various classes in order to reduce
code duplication and keep the other classes cleaner in terms of what code they
contain.

Additionaly the root of the project contains two directories of interest:

- `db` contains the database schema and test data migrations used to populate
  the database

- `utils` contains various utilities for running / building the application

### Virtual Threads

There is perhaps no more exciting and potentially game changing feature released
in the java language recently than Virtual Threads as introduced in Java 21. For
web applications such as this, it was very important not to block the web server
threads, which your requests came in on. If you did, you would very quickly stop
being able to serve incoming requests as load scaled up. The traditional
approach to dealing with this was either to create your own thread pool and
create and submit threads, or use Futures to offload requests to a separate
async thread pool. Occasionally using an "actor" library like Akka was also
chosen. Managing your own threads was expensive and since Java threads mapped
1:1 to system threads, they were much more limited than a lot of high
performance applications needed.

Futures would be scheduled to run on a set of Java threads, but would cleanly
release the thread to do other work when the future was blocked. This was
usually the preferred form of creating performant, asyncronous web applications.
The biggest downside to using futures was that it made reading and tracing the
code more difficult, and it made stack traces for errors much less useful.

Consider the following stack trace:

```java
java.lang.RuntimeException: Surprise error
        at com.ncitguys.poster.db.repository.CommentRepository.findByPostId(CommentRepository.java:98) ~[main/:na]
        at com.ncitguys.poster.controller.PostController.lambda$1(PostController.java:98) ~[main/:na]
        at java.base/java.util.concurrent.CompletableFuture$AsyncSupply.run(CompletableFuture.java:1768) ~[na:na]
        at java.base/java.util.concurrent.CompletableFuture$AsyncSupply.exec(CompletableFuture.java:1760) ~[na:na]
        at java.base/java.util.concurrent.ForkJoinTask.doExec(ForkJoinTask.java:387) ~[na:na]
        at java.base/java.util.concurrent.ForkJoinPool$WorkQueue.topLevelExec(ForkJoinPool.java:1312) ~[na:na]
        at java.base/java.util.concurrent.ForkJoinPool.scan(ForkJoinPool.java:1843) ~[na:na]
        at java.base/java.util.concurrent.ForkJoinPool.runWorker(ForkJoinPool.java:1808) ~[na:na]
        at java.base/java.util.concurrent.ForkJoinWorkerThread.run(ForkJoinWorkerThread.java:188) ~[na:na]
```

We can see that the exception was thrown by the `findByPostId` method in the
`CommentRepository` and we can see that was called by some lambda in the
`PostController`, but we don't know which one. In this simple application that
might be enough information, but in a more complex application, futures tend
to call other futures and the call trace gets muddied very quickly. For example
in this stack trace, the completable future throws an error after the list comes
back from the repository but before returning the result to Spring to send back
to the user. Now we only know that the exception was thrown by a lambda in the
`PostController`, but if there were multiple ways to get to this point, we'd
have no idea how we got here.

```java
java.lang.RuntimeException: Surprise!
        at com.ncitguys.poster.controller.PostController.lambda$2(PostController.java:100) ~[main/:na]
        at java.base/java.util.concurrent.CompletableFuture$UniApply.tryFire(CompletableFuture.java:646) ~[na:na]
        ...
```

Now compare those to a stack trace from a Virtual Thread (some lines removed):

```java
java.lang.RuntimeException: Surprise Error!
        at com.ncitguys.poster.db.repository.CommentRepository.findByPostId(CommentRepository.java:98) ~[main/:na]
        at com.ncitguys.poster.controller.PostController.getComments(PostController.java:97) ~[main/:na]
        at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103) ~[na:na]
        at java.base/java.lang.reflect.Method.invoke(Method.java:580) ~[na:na]
        at org.springframework.aop.support.AopUtils.invokeJoinpointUsingReflection(AopUtils.java:351) ~[spring-aop-6.1.4.jar:6.1.4]
        at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:713) ~[spring-aop-6.1.4.jar:6.1.4]
        at com.ncitguys.poster.controller.PostController$$SpringCGLIB$$0.getComments(<generated>) ~[main/:na]
        at java.base/jdk.internal.reflect.DirectMethodHandleAccessor.invoke(DirectMethodHandleAccessor.java:103) ~[na:na]
        at java.base/java.lang.reflect.Method.invoke(Method.java:580) ~[na:na]
        at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:259) ~[spring-web-6.1.4.jar:6.1.4]
        ...
        at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:896) ~[tomcat-embed-core-10.1.19.jar:10.1.19]
        at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1744) ~[tomcat-embed-core-10.1.19.jar:10.1.19]
        at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:52) ~[tomcat-embed-core-10.1.19.jar:10.1.19]
        at java.base/java.lang.VirtualThread.run(VirtualThread.java:309) ~[na:na]
```

This looks much more like a stack trace we would expect to see in any other java
application. We have the full method call stack all the way from the beginning
of the Spring application right down to all the calls in our application. All
Virtual Threads maintain their own call stacks even as they are moved from
system thread to system thread. This means that as developers we can write code
exactly as we would in a single threaded application, and get all of the
benefits of a single thread as far as readability and tracability while still
getting async performance.

By enabling Virtual Threads in Spring, all of our requests automatically come in
on a Virtual Thread. We no longer have to remember to offload our application
work to a future or our own managed thread. Additionally we can take advantage
of things like the MDC in the SLF4J logging framework. The MDC allows us to set
common context values on a per thread basis. Like for a given request we could
add a trace ID, or add the post ID of the request and every log message we print
using the logging framework would have those values attached without us having
to remember to attach it each time. In a Futures world, we would need to retain
the context through the call stacks and add that context before sending each
log message and then remove it again, because Futures did not behave as a thread
and did not keep their own thread contexts. Even the thread ID of a Virtual
Thread remains constant for the entire time the Virtual Thread is active,
regardless of which actual platform thread it is running on.

There are still a few caveats to using Virtual Threads particularly around using
`synchronized` method calls and their associated monitors. There are possible
work arounds for those items and the Java development teams are already
working to provide automatic resolution for that concern. At this point any new
Java application should default to Virtual Threads for its concurrency model
and only chose a different path if measured performance numbers indicate that
an alternative is warranted. Otherwise Virtual Threads should be considered a
nearly free and instance performance and quality of life improvement.

### Java Records

Java Record classes simplify the creation of largely immutable data classes.
They work as great lightweight objects for transferring data through the app
and because they are immutable, we can be assured* that the data doesn't get
changed underneath us as the data is passed through the system. In addition,
records are automatically and properly serialized and deserialized by Jackson
and Spring. For these reasons, for any basic data classes, records should be
considered the default option. We do use [Lombok](#lombok) to add Builder
capabilities to the records as that has not been implemented yet. However, the
builder pattern comes with the caveat that it is possible to add new fields to
a record and not update your builder to add the new field where they are being
created. To solve that, all new records are created by way of the canonical
record contstructor so that the compiler will inform us if we're not correctly
setting all fields. Builders are only used in places where we want to make a
copy of an existing record and change one or two fields from the original.

### Final Variables

You will note that almost all variables in the code are marked as `final`. This
is automatic behavior of the IDE code cleanup that I've been writing this demo
in. Normally I would prefer to only mark class members as final where they can
be, but not mark method parameters or intermediate local variables as I find
it to largely be noise relative to the benefits it provides us. I did not take
the time to find where to disable that particular cleanup for this demo.

### Dependencies

#### Spring Boot

While Spring is a heavy framework especially for so simple of a backend,
it is a well tested and widely used framework. Once you have a handle on its
configuration is significantly reduces the amount of code developers have to
write and maintain and provides access to a number of business production
features. Ultimately, the choice of a different framework should be made only
if measured testing shows an improvement or if other external requirements
impose restrictions on using Spring.

#### Lombok

Project Lombok is a controversial dependency and annotation processor. Some Java
developers look at it as a hack, or a completely different language built on top
of java and the JVM. Notably some of Java's own developers hold this view. They
cite the fact that Lombok often uses private or unexposed methods of modifying
the java byte code and that prior JVM upgrades have left projects broken until
Lombok was able to be upgraded. They also point out that the `de-lombok`
functionality of Lombok has not always worked as expected.

On the other hand, a number of developers find this risks to be acceptable in
light of the amount of savings that Lombok provides. As a non-runtime dependency
Lombok only imposes any real concerns at compile time. While other libraries
like `Immutables` provide various similar features to Lombok, very few provide
all the functionality and in many cases add their own extra overhead (e.g.
Immutables requires defining abstract classes for all of your immutable types
for which entirely new classes will be generated). While Java's new `record`'s
have made a significant dent in adding native functionality similar to Lombok's
`@Data` and `@Value` annotations, there is still a ways to go and Lombok still
provides a notable value to a developer.

For this project, the risks of Lombok are judged to be an acceptable risk
relative to the benefits and so Lombok has been added as a dependency.

#### Testcontainers

Testcontainers is a dependency that allows us to spin up a real PostgreSQL
database for testing purposes. It does this by way of docker, which does
introduce a dependency in our testing pipeline on docker. On the one hand this
requires us to have an active internet connection to pull the docker image.
However, this provides us a number of benefits over using in in memory DB like
H2.

1. We can run our integration tests against the real database we're going to
   use for production.

2. We can as a result of #1 use our real database migration scripts to set up
   the initial DB schemas. This ensures our schema migrations keep up to date
   with our code changes.

3. We don't need alternative SQL or to limit ourselves only to shared SQL
   between our test DB and the production DB. For example, in our queries we
   are using the `RETURNING *` clause available in Postgres to return the
   entire newly inserted row in a table without needing to issue a second
   `SELECT` statement. H2 doesn't support this functionality, so in order to
   run the integration tests, we would either need to change our SQL to use
   only the functionality H2 and PostgreSQL support together, or we need to
   have custom test SQL which defeats the purpose of running the integration
   test.

Given that modern build and dependency management such as Gradle and Maven
already assume the existence of a network for pulling down any dependecies not
in the local cache, this isn't a large of an imposition as it might first
appear.

Ensuring that docker is available on our build/test machines is an extra step
to be sure, but one that is well worth it to be able to run integration tests
entirely from the Java working.

### Tests

#### Distinct static values for various mock test data

Overall, using as little mocking as possible tends to be best for testing as it
means you're exercising the most real code. To that end, one of the important
things to be testing is that your arguments of the same type are being passed
to the right places. So when generating test data, we create a number of static
variables that normally would just be inline "magic" values. If we make sure
each of these is a unique value, we can be assured if we're using the right
`verify` methods that we're passing the right arguments to the right places.
For example if we had two classes like so:

```java

class InnerClass {
    //...
    public void bar(int fizz, int buzz) {
        //...
    }
    //...
}
class ClassUnderTest {
    InnerClass innerClass = new InnerClass();
    //...
    public foo(int fizzy, int buzzy) {
        //...
        bar(buzz, fizz);
        //...
    }
    //...
}
```

And some tests constructed like this, we wouldn't learn from our tests that
`foo` swaps the arguments when bar is called:

```java

@Test
public void fooSwapped() {
    classUnderTest.fooSwapped(1, 1);
    // This works just fine
    verify(mockInnerClass).bar(1,1);
}
```

But if we create unique variables and ensure we're verifying the associated
arguments, we can catch these mistakes in our tests.

```java

@Test
public void fooSwapped() {
    static int FIZZ = 1;
    static int BUZZ = 10;
    classUnderTest.fooSwapped(FIZZ, BUZZ);
    // This will fail because we're swapping the
    // arguments in the class under test
    verify(mockInnerClass).bar(FIZZ, BUZZ);
}
```

### REST Behavior

#### Missing Administrative Endpoints

Depending on the specific requirements of the application, it may be desirable
to also implement the following endpoints:
`DELETE /admin/posts/{id}/comments` to delte all comments on a post and

`DELETE /admin/comments/{id}/all` to delete the entire tree of replies for a
given comment.

`GET /admin/posts/{id}` to unconditionally get a post regardless of it's
published state

`GET /admin/posts/{id}/comments` to get all comments for a given post regardless
of their published state

`GET /admin/comments/{id}` to get a comment regardless of it's published state

`GET /posts/{id}/comments/{commentId}` to get a single comment from a post, perhaps for making a replay page that just shows the comment being replied to, or to allow users to permanently link to individual comments.

Additionally the endpoint that retrieves all posts by an author might be better
expanded into an endpoint for querying posts in general, with query parameters
for searching

#### Errors on Delete

Whether or not an endpoint should report an error on an attempted deletion that
is invalid is often a matter of design choices rather than a specific REST
best practice. One school of thought says that if the system can't do what the
user has requested for any reason, it should inform the user. In this demo, we
have chosen to follow this school of thought and to report errors on invalid
deletions for the user endpoints. It would have been just as valid to choose to
report a `200 OK` on the philosophy that a delete should be idempotent and
should be considered successful if the item is already deleted.

For the admin endpoints, we chose a the other philosphy and deletes for items
that don't exist will report a successful result. This choice was made largely
on the idea that because the admin edpoints really remove from the DB as
opposed to changing a flag, if the item isn't in the DB, that's the intended
state anyway.

### Data Security

#### Hard Coded Authentication for Admin Endpoints

For the purposes of the demo, the admin endpoints are only protected with hard
coded plaintex passwords and the default encryptor. This is NOT something that
should be done in production and a proper production setup should pre-encrypt
passwords and load them from a separate config.

#### Returning raw DTO objects

The various repositories used by the controller return raw records that map
1:1 with the data in the database. This means we don't need a public facing
record and a private internal record that we must map back and forth between.
This might be acceptabe if the database does not contain any private data for
these records or there are no concerns about exposing database column names to
clients. However, in many real world production applications, the customer's
front end view would be much more restricted than a full view into the DTO.

Springs Data JDBC access implementations do allow us to generate "views" into
the returned data from a query, or we would want to make a separate set of front
end DTO objects that represent the public facing API for outbound data, similar
to the classes in the `api` package that represent incoming request data.

### Database Access

#### Raw JDBC Named Templates vs Spring Data JDBC access

The `PostRepository`uses the Spring Data JDBC access annotations. These
annotations vastly reduce the amount of extra code we have to write, both live
code and testing code. This is at the possible expense of not having as much
control over how the spring transactions are interacting with the database and
method names that can be a bit obtuse. Because we can define custom SQL queries
by handthis represents a nice middle ground between a heavyweight ORM like
Hibernate and hand writing all of our own SQL with raw JDBCTemplate calls.

However, there are some bugs in spring's implementation. Notably at the time of
this writing, [derrived delete queries are a little broken][spring-delete]. As
a result, for the `CommentRepository` we have taken the more traditional JDBC
Template query approach. We can see increased amount of code we must write and
maintain. And as noted, in a heavily loaded system, spring's JDBC system may
increase DB load when it does things like query to return the new values of
an item when updated, even if we're not going to return the updated values.

### Database Schema

#### IDs

IDs for posts and comments are both defined as INT types. This allows for a
[maximum value of 2147483647][max-int]. With sufficient traffic this could pose
an overflow issue and a monitor should be set up to warn about approaching the
limit and a decision about future conversion.

One option would be to upgrade from an INT to a BIGINT which would [max out at
9223372036854775807][max-int] and be a simple update to the column type with
minimal need to adjust code (DAOs would probably need to switch to using `long`
data types).

Another option would be to convert to using UUID's for IDs. UUIDs have the
benefit of being unique, un-guessable, practically infinite and extremely
unlikely to experience collisions if the data needed to be merged with another
table. UUIDs largely suffer from being poorly indexable (if using random UUIDs),
and that could have performance impacts. Testing and performance measurements
would need to be performed to assess the impact on the specific application to
determine if switching to UUIDs would harm the application. Additionally by the
time this evaluation is necessary, the [UUID7][uuid7] spec may be finalized
which aims to create a UUID format that resolves the indexing concerns.

#### Foreign Keys

The `comments` table specifies two Foreign Key constraints. `comment_on` depends
on there being a valid post id in the `posts` table that the comment is attached
to. If no such post exists, the database will not allow us to insert the record.
Likewise, `reply_to` depends on there being a valid parent comment that the newly
inserted comment is a reply to.

This moves some degree of data integrity validation to the database, which is a
tradeoff that allows multiple client applications to write to the DB without
violating that constraint, but with the consequence that it requires clients to
update the database in a specific order, and might prevent administrative
decisions (such as setting the values to a "magic" number that has unique
meaning to the application).

Additionally in a world where the DB might need to be operated in an
"eventually consistent" model, these constraints require some coordination and
serialization. This isn't likely to be a concern in a case like this where a
user must be able to read a post or comment to reply (and thus is must already
be in the database), but there are application designs where such a concern is
relevant and it should be kept in mind.

#### Foreign Keys Can Be Null

The decision to allow the foreign keys to be null allows for a post to be
permanently deleted from the DB without losing the comments that were written;
or alternatively for a comment to be deleted without the replies becoming lost.
This can lead to invalid data (comments without an associated post, replies that
referenced un-viewable parents) but it provides administrative flexibility and
minimizes data loss in the event of a mistaken deletion.

One alternative would be to either specify a `CASCADE` on delete, which would
remove all the comments referencing a post if the post is deleted or all the
replies to a comment if the parent comment is deleted. This keeps the data
integrity high, and reduces un-usable data in the database at the risk of losing
a large swath of data in the event of an accidental deletion. Good backups would
become critical if allowing `CASCADE` deletes, and the use of WAL archives and
Postges' "Point-In-Time" recovery feature should be strongly considered.

The other most common alternative would be to `DENY` the deletes. This prevents
accidental deletion, with the cost that an intentional deletion of a given post
or comment requires first deleting all of its referents.

#### Authors as Strings

In a real world / multi-user environment, authors of posts and comments should
be their own entities in the database in a `users` table, with proper unique IDs
and additional data as needed normalized out. As providing login functionality
was outside the scope of this project, authors are instead simple strings in
each table.

### Remaining Work for a Production Release

Before releasing an application like this to production there are a number of
unimplemented items that should be done:

1. More comprehensive logging should be in place throughout the application to
   make tracing issued easier

2. The WebSecurity configuration should be switched to a secure and encrypted
   version

3. A decision would need to be made about whether the application will use SSL
   and if it will, whether that SSL will be provided with certificates stored in
   the java keystore and using the built in server options, or if a forwarding
   proxy will be deployed along with the application that will provide SSL via
   Apache or NGINX, and forward requests to a non-public unencrypted port that
   the application is running on

4. An OpenAPI spec or other documentation of the user facing API should be
   created and publshed. This is where the decision to use a separate set of API
   objects for return values rather than the database DTOs would likely be made
   since that allows changing the underlying databse model without changing the
   front end API. If the DB DTOs were used, it would still be possible to
   replace them with API objects later, but that work might be better done up
   front now

5. Metrics and mointoring information should be published. While often related
   to logging, Spring enables publishing a number of metrics to various metric
   aggregation frontends. This would allow us to monitor things like request
   processing times and counts, error rates, database connection speeds etc.

6. Connection pooling configuration for the database. While Spring defaults to
   using a basic Hikari pool for database connections, further consideration and
   tuning of that pool should be done, as well as evaluating other pooling
   options like C3PO. Additionally while the application provides its own
   internal pool, having a pooling frontend for the database itself like
   PgPool-II or PgBouncer would allow other applications that don't use native
   pooling to connect to the same database and still benefit from connection
   pools. They also would allow things like replicated connections behind a
   single endpoint and load balancing between those replicated databases.
   Similar functionality is available via RDS as well.

7. The creation of a production spring profile with proper production
   credations and settings

8. A set of automated build and deploy pipelines should be created to reduce the
   amount of manual work needed to release new changes and reduce the risk of
   accidentally breaking something in production

[max-int]: https://www.postgresql.org/docs/current/datatype-numeric.html
[spring-delete]: https://github.com/spring-projects/spring-data-relational/issues/771
[uuid7]: https://datatracker.ietf.org/doc/draft-ietf-uuidrev-rfc4122bis/
