-- Create the table for posts
CREATE TABLE posts (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    posted_date TIMESTAMP WITH TIME ZONE NOT NULL,
    last_edited TIMESTAMP WITH TIME ZONE NOT NULL CHECK (last_edited >= posted_date),
    author CHARACTER VARYING(2048) NOT NULL,
    content TEXT NOT NULL,
    published BOOLEAN NOT NULL DEFAULT TRUE
);

-- Indexes for the posts table
-- posted_date because posts will be displayed in a given order
--   and may be paginated on that in future designs
-- published as a flag to determine whether or not to display a given post
CREATE INDEX ON posts (posted_date DESC NULLS LAST);
CREATE INDEX ON posts (published);


-- Create the table for comments
CREATE TABLE comments (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    comment_on INT REFERENCES posts ON DELETE SET NULL,
    reply_to INT,
    posted_date TIMESTAMP WITH TIME ZONE NOT NULL,
    last_edited TIMESTAMP WITH TIME ZONE NOT NULL CHECK (last_edited >= posted_date),
    author CHARACTER VARYING(2048) NOT NULL,
    content TEXT NOT NULL,
    published BOOLEAN NOT NULL DEFAULT TRUE,
    UNIQUE (comment_on, id),
    FOREIGN KEY (comment_on, reply_to) REFERENCES comments (comment_on, id) ON DELETE SET NULL (reply_to)
);

-- Indexes for the comments table
-- reply_to because we're going to need to get a hierarchy of comments
-- published as a flag to determine whether a comment should be displayed at all
-- comment_on to narrow down a set of comments to a given post
CREATE INDEX ON comments(reply_to);
CREATE INDEX ON comments (published);
CREATE INDEX ON comments (comment_on);

COMMENT ON COLUMN comments.comment_on IS
    'The ID of the post this comment is attached to. Should only ever be null if the original post was deleted';
COMMENT ON COLUMN comments.reply_to IS
    'If this comment is a reply to another comment, this is the ID of that parent comment. Should be null only if this is a top level comment or if the parent comment was deleted';
