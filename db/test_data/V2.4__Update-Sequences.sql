-- Update the ID sequences since we manually set them
-- in the prior data migrations, otherwise if we try to
-- post new updates to the application, we'll get duplicate
-- key violations until we send enough requests to get
-- to a number we haven't already used.
ALTER SEQUENCE posts_id_seq RESTART WITH 4;
ALTER SEQUENCE comments_id_seq RESTART WITH 316;
